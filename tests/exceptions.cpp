//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"

#include <tinynuget/exceptions.h>

#include <gtest/gtest.h>

namespace tinynuget::test
{
    ////////////////////////////////////////////////////////////////////
    TEST(TinyNuget_Exceptions_UnsupportedProtocolException, Version)
    {
        UnsupportedProtocolException exc{ProtocolVersion::v3};
        EXPECT_EQ(ProtocolVersion::v3, exc.version());
    }

    TEST(TinyNuget_Exceptions_UnsupportedProtocolException, What)
    {
        const char* message = nullptr;

        {
            UnsupportedProtocolException exc{ProtocolVersion::v3};
            message = exc.what();
        }

        EXPECT_STREQ("UnsupportedProtocolException[version=3]", message);
    }

    TEST(TinyNuget_Exceptions_UnsupportedFeedException, Version)
    {
        UnsupportedFeedException exc{"https://www.nuget.org/api/v2"};
        EXPECT_EQ("https://www.nuget.org/api/v2", std::string{exc.address()});
    }

    TEST(TinyNuget_Exceptions_UnsupportedFeedException, What)
    {
        const char* message = nullptr;

        {
            UnsupportedFeedException exc{"https://www.nuget.org/api/v2"};
            message = exc.what();
        }

        EXPECT_STREQ("UnsupportedFeedException[address=https://www.nuget.org/api/v2]", message);
    }

    TEST(TinyNuget_Exceptions_VersionNotFoundException, Name)
    {
        VersionNotFoundException exc{"newtonsoft.json", *VersionRange::parse("[1.2.3]")};
        EXPECT_EQ("newtonsoft.json", std::string{exc.name()});
    }

    TEST(TinyNuget_Exceptions_VersionNotFoundException, Range)
    {
        VersionNotFoundException exc{"newtonsoft.json", *VersionRange::parse("[1.2.3]")};
        EXPECT_STREQ("1.2.3 <= version <= 1.2.3", exc.range().toString().c_str());
    }

    TEST(TinyNuget_Exceptions_VersionNotFoundException, What)
    {
        const char* message = nullptr;

        {
            VersionNotFoundException exc{"newtonsoft.json", *VersionRange::parse("[1.2.3]")};
            message = exc.what();
        }

        EXPECT_STREQ("VersionNotFoundException[name='newtonsoft.json', range='1.2.3 <= version <= 1.2.3']", message);
    }

    ////////////////////////////////////////////////////////////////////
    TEST(TinyNuget_Exceptions_NSErrorException, What)
    {
        const char* message = nullptr;

        {
            NSErrorException exc;
            exc.code = -3453245723457;
            exc.domain = "NSDummyErrorDomain";
            exc.description = "The quick brown fox jumps over the lazy dog.";

            message = exc.what();
        }

        EXPECT_STREQ("NSErrorException[code=-3453245723457, domain=NSDummyErrorDomain, desc='The quick brown fox jumps over the lazy dog.']",
                     message);
    }

    TEST(TinyNuget_Exceptions_CurlException, What)
    {
        const char* message = nullptr;

        {
            CurlException exc;
            exc.code = -2345435;
            exc.description = "The quick brown fox jumps over the lazy dog.";

            message = exc.what();
        }

        EXPECT_STREQ("CurlException[code=-2345435, desc='The quick brown fox jumps over the lazy dog.']", message);
    }

    TEST(TinyNuget_Exceptions_WinHttpException, What)
    {
        const char* message = nullptr;

        {
            WinHttpException exc;
            exc.code = 2345435;
            exc.description = "The quick brown fox jumps over the lazy dog.";

            message = exc.what();
        }

        EXPECT_STREQ("WinHttpException[code=2345435, desc='The quick brown fox jumps over the lazy dog.']", message);
    }

    TEST(TinyNuget_Exceptions_HttpStatusException, What)
    {
        const char* message = nullptr;

        {
            HttpStatusException exc;
            exc.resourceUrl = "https://google.ca/something";
            exc.statusCode = 502;

            message = exc.what();
        }

        EXPECT_STREQ("HttpStatusException[resource='https://google.ca/something', statusCode=502]", message);
    }

    TEST(TinyNuget_Exceptions_JsonFormatException, What)
    {
        const char* message = nullptr;

        {
            JsonFormatException exc;
            exc.description = "The quick brown fox jumps over the lazy dog.";

            message = exc.what();
        }

        EXPECT_STREQ("JsonFormatException[desc='The quick brown fox jumps over the lazy dog.']", message);
    }

    TEST(TinyNuget_Exceptions_XmlFormatException, What)
    {
        const char* message = nullptr;

        {
            XmlFormatException exc;
            exc.code = -345345;
            exc.description = "The quick brown fox jumps over the lazy dog.";

            message = exc.what();
        }

        EXPECT_STREQ("XmlFormatException[code=-345345, desc='The quick brown fox jumps over the lazy dog.']", message);
    }

} // namespace tinynuget::test

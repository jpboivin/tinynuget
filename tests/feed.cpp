//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/client.h>
#include <tinynuget/exceptions.h>
#include <tinynuget/feed.h>
#include <tinynuget/package.h>
#include <tinynuget/version_range.h>

#include <chrono>

#include <gtest/gtest.h>

using namespace std::chrono_literals;

namespace tinynuget::test
{
    TEST(TinyNuget_Feed, DefaultFeedAddress_V2)
    {
        EXPECT_STREQ("https://www.nuget.org/api/v2/", std::string{default_v2_feed_address}.c_str());
    }

    TEST(TinyNuget_Feed, DefaultFeedAddress_V3)
    {
        EXPECT_STREQ("https://api.nuget.org/v3/", std::string{default_v3_feed_address}.c_str());
    }

    TEST(TinyNuget_Feed, DefaultFeedAddress)
    {
        EXPECT_STREQ("https://api.nuget.org/v3/", std::string{default_feed_address}.c_str());
    }

    TEST(TinyNuget_Feed, ResolvePackage_V2)
    {
        Feed feed = Client{}.openFeed(default_v2_feed_address, ProtocolVersion::v2).get();

        auto resolveTask = feed.resolvePackage("Newtonsoft.Json", *VersionRange::parse("[10.0.1]"));
        resolveTask.wait_for(30s);

        [[maybe_unused]] auto package = resolveTask.get();
        EXPECT_STREQ("newtonsoft.json", std::string{package.name()}.c_str());
        EXPECT_STREQ("10.0.1", package.version().normalize().c_str());
    }

    TEST(TinyNuget_Feed, ResolvePackage_V2_MultipleMatchingVersions)
    {
        Feed feed = Client{}.openFeed(default_v2_feed_address, ProtocolVersion::v2).get();

        auto resolveTask = feed.resolvePackage("Newtonsoft.Json", *VersionRange::parse("[10,11)"));
        resolveTask.wait_for(30s);

        [[maybe_unused]] auto package = resolveTask.get();
        EXPECT_STREQ("newtonsoft.json", std::string{package.name()}.c_str());
        EXPECT_STREQ("10.0.3", package.version().normalize().c_str()); // latest 10.*
    }

    TEST(TinyNuget_Feed, ResolvePackage_V2_PackageNotFound)
    {
        Feed feed = Client{}.openFeed(default_v2_feed_address, ProtocolVersion::v2).get();

        auto resolveTask = feed.resolvePackage("Dummy.Package.That.Wont.Exist", *VersionRange::parse("[1.0.0]"));
        resolveTask.wait_for(30s);

        try
        {
            resolveTask.get();
            GTEST_FAIL() << "get() did not throw PackageNotFoundException as expected.";
        }
        catch (PackageNotFoundException& exc)
        {
            EXPECT_STREQ("dummy.package.that.wont.exist", std::string{exc.name()}.c_str());
        }
    }

    TEST(TinyNuget_Feed, ResolvePackage_V2_VersionNotFound)
    {
        Feed feed = Client{}.openFeed(default_v2_feed_address, ProtocolVersion::v2).get();

        auto resolveTask = feed.resolvePackage("Newtonsoft.Json", *VersionRange::parse("[99.99.99]"));
        resolveTask.wait_for(30s);

        try
        {
            resolveTask.get();
            GTEST_FAIL() << "get() did not throw VersionNotFoundException as expected.";
        }
        catch (VersionNotFoundException& exc)
        {
            EXPECT_STREQ("newtonsoft.json", std::string{exc.name()}.c_str());
            EXPECT_STREQ("99.99.99 <= version <= 99.99.99", exc.range().toString().c_str());
        }
    }

    TEST(TinyNuget_Feed, ResolvePackage_V3)
    {
        Feed feed = Client{}.openFeed(default_v3_feed_address, ProtocolVersion::v3).get();

        auto resolveTask = feed.resolvePackage("Newtonsoft.Json", *VersionRange::parse("[10.0.1]"));
        resolveTask.wait_for(30s);

        [[maybe_unused]] auto package = resolveTask.get();
        EXPECT_STREQ("newtonsoft.json", std::string{package.name()}.c_str());
        EXPECT_STREQ("10.0.1", package.version().normalize().c_str());
    }

    TEST(TinyNuget_Feed, ResolvePackage_V3_MultipleMatchingVersions)
    {
        Feed feed = Client{}.openFeed(default_v3_feed_address, ProtocolVersion::v3).get();

        auto resolveTask = feed.resolvePackage("Newtonsoft.Json", *VersionRange::parse("[10,11)"));
        resolveTask.wait_for(30s);

        [[maybe_unused]] auto package = resolveTask.get();
        EXPECT_STREQ("newtonsoft.json", std::string{package.name()}.c_str());
        EXPECT_STREQ("10.0.3", package.version().normalize().c_str()); // latest 10.*
    }

    TEST(TinyNuget_Feed, ResolvePackage_V3_PackageNotFound)
    {
        Feed feed = Client{}.openFeed(default_v3_feed_address, ProtocolVersion::v3).get();

        auto resolveTask = feed.resolvePackage("Dummy.Package.That.Wont.Exist", *VersionRange::parse("[1.0.0]"));
        resolveTask.wait_for(30s);

        try
        {
            resolveTask.get();
            GTEST_FAIL() << "get() did not throw PackageNotFoundException as expected.";
        }
        catch (PackageNotFoundException& exc)
        {
            EXPECT_STREQ("dummy.package.that.wont.exist", std::string{exc.name()}.c_str());
        }
    }

    TEST(TinyNuget_Feed, ResolvePackage_V3_VersionNotFound)
    {
        Feed feed = Client{}.openFeed(default_v3_feed_address, ProtocolVersion::v3).get();

        auto resolveTask = feed.resolvePackage("Newtonsoft.Json", *VersionRange::parse("[99.99.99]"));
        resolveTask.wait_for(30s);

        try
        {
            resolveTask.get();
            GTEST_FAIL() << "get() did not throw VersionNotFoundException as expected.";
        }
        catch (VersionNotFoundException& exc)
        {
            EXPECT_STREQ("newtonsoft.json", std::string{exc.name()}.c_str());
            EXPECT_STREQ("99.99.99 <= version <= 99.99.99", exc.range().toString().c_str());
        }
    }

} // namespace tinynuget::test

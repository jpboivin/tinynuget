//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/version_range.h>

#include <gtest/gtest.h>

namespace tinynuget::test
{
    TEST(TinyNuget_VersionRange, Parse_Valid)
    {
        for (std::string_view range : {
                 "*",           "1.*",       "1.2.*",     "1",         "1.2",       "1.2.3",       "(1,)",        "(1.2,)",
                 "(1.2.3,)",    "[1]",       "[1.2]",     "[1.2.3]",   "(,1]",      "(,1.2]",      "(,1.2.3]",    "(,1)",
                 "(,1.2)",      "(,1.2.3)",  "[1,4]",     "[1.2,4]",   "[1.2.3,4]", "[1,4.5]",     "[1,4.5.6]",   "[1.2,4.5]",
                 "[1.2,4.5.6]", "(1,4)",     "(1.2,4)",   "(1.2.3,4)", "(1,4.5)",   "(1,4.5.6)",   "(1.2,4.5)",   "(1.2,4.5.6)",
                 "(1,4]",       "(1.2,4]",   "(1.2.3,4]", "(1,4.5]",   "(1,4.5.6]", "(1.2,4.5]",   "(1.2,4.5.6]", "[1,4)",
                 "[1.2,4)",     "[1.2.3,4)", "[1,4.5)",   "[1,4.5.6)", "[1.2,4.5)", "[1.2,4.5.6)",
             })
        {
            EXPECT_TRUE(VersionRange::parse(range)) << "Valid Range: " << std::string{range}.c_str();
        }
    }

    TEST(TinyNuget_VersionRange, Parse_Invalid)
    {
        for (std::string_view range : {
                 "",
                 "abc",
                 "1.*.0",
                 "(1.0)",
                 "(1.*,)",
                 "(,1.*)",
                 "[1.*]",
                 "[1.*,2.0]",
                 "[1.0,2.*]",
                 "(1.*,2.0]",
                 "(1.0,2.*]",
                 "[1.*,2.0)",
                 "[1.0,2.*)",
                 "[1.0",
                 "1.0]",
             })
        {
            EXPECT_FALSE(VersionRange::parse(range)) << "Invalid Range: " << std::string{range}.c_str();
        }
    }

    TEST(TinyNuget_VersionRange, In)
    {
        SemanticVersion version099{0, 9, 9};
        SemanticVersion version100{1, 0, 0};
        SemanticVersion version101{1, 0, 1};
        SemanticVersion version200{2, 0, 0};
        SemanticVersion version201{2, 0, 1};

        // 1.0      x ≥ 1.0     Minimum version, inclusive
        {
            VersionRange range = VersionRange::parse("1.0").value();

            EXPECT_FALSE(range.in(version099));
            EXPECT_TRUE(range.in(version100));
            EXPECT_TRUE(range.in(version101));
            EXPECT_TRUE(range.in(version200));
            EXPECT_TRUE(range.in(version201));
        }

        // (1.0,)    x ≥ 1.0     Minimum version, exclusive
        {
            VersionRange range = VersionRange::parse("(1.0,)").value();

            EXPECT_FALSE(range.in(version099));
            EXPECT_FALSE(range.in(version100));
            EXPECT_TRUE(range.in(version101));
            EXPECT_TRUE(range.in(version200));
            EXPECT_TRUE(range.in(version201));
        }

        // [1.0]    x == 1.0    Exact version match
        {
            VersionRange range = VersionRange::parse("[1.0]").value();

            EXPECT_FALSE(range.in(version099));
            EXPECT_TRUE(range.in(version100));
            EXPECT_FALSE(range.in(version101));
            EXPECT_FALSE(range.in(version200));
            EXPECT_FALSE(range.in(version201));
        }

        // (,1.0]    x ≤ 1.0    Maximum version, inclusive
        {
            VersionRange range = VersionRange::parse("(,1.0]").value();

            EXPECT_TRUE(range.in(version099));
            EXPECT_TRUE(range.in(version100));
            EXPECT_FALSE(range.in(version101));
            EXPECT_FALSE(range.in(version200));
            EXPECT_FALSE(range.in(version201));
        }

        // (,1.0)    x < 1.0    Maximum version, exclusive
        {
            VersionRange range = VersionRange::parse("(,1.0)").value();

            EXPECT_TRUE(range.in(version099));
            EXPECT_FALSE(range.in(version100));
            EXPECT_FALSE(range.in(version101));
            EXPECT_FALSE(range.in(version200));
            EXPECT_FALSE(range.in(version201));
        }

        // [1.0,2.0]    1.0 ≤ x ≤ 2.0    Exact range, inclusive
        {
            VersionRange range = VersionRange::parse("[1.0,2.0]").value();

            EXPECT_FALSE(range.in(version099));
            EXPECT_TRUE(range.in(version100));
            EXPECT_TRUE(range.in(version101));
            EXPECT_TRUE(range.in(version200));
            EXPECT_FALSE(range.in(version201));
        }

        // (1.0,2.0)    1.0 < x < 2.0    Exact range, exclusive
        {
            VersionRange range = VersionRange::parse("(1.0,2.0)").value();

            EXPECT_FALSE(range.in(version099));
            EXPECT_FALSE(range.in(version100));
            EXPECT_TRUE(range.in(version101));
            EXPECT_FALSE(range.in(version200));
            EXPECT_FALSE(range.in(version201));
        }

        // [1.0,2.0)    1.0 ≤ x < 2.0    Mixed inclusive minimum and exclusive maximum version
        {
            VersionRange range = VersionRange::parse("[1.0,2.0)").value();

            EXPECT_FALSE(range.in(version099));
            EXPECT_TRUE(range.in(version100));
            EXPECT_TRUE(range.in(version101));
            EXPECT_FALSE(range.in(version200));
            EXPECT_FALSE(range.in(version201));
        }
    }

    TEST(TinyNuget_VersionRange, ToString)
    {
        EXPECT_STREQ("0.0.0 <= version", VersionRange::parse("*")->toString().c_str());
        EXPECT_STREQ("1.0.0 <= version < 2.0.0", VersionRange::parse("1.*")->toString().c_str());
        EXPECT_STREQ("1.2.0 <= version < 1.3.0", VersionRange::parse("1.2.*")->toString().c_str());
        EXPECT_STREQ("1.0.0 <= version", VersionRange::parse("1.0")->toString().c_str());
        EXPECT_STREQ("1.0.0 < version", VersionRange::parse("(1.0,)")->toString().c_str());
        EXPECT_STREQ("1.0.0 <= version <= 1.0.0", VersionRange::parse("[1.0]")->toString().c_str());
        EXPECT_STREQ("version <= 1.0.0", VersionRange::parse("(,1.0]")->toString().c_str());
        EXPECT_STREQ("version < 1.0.0", VersionRange::parse("(,1.0)")->toString().c_str());
        EXPECT_STREQ("1.0.0 <= version <= 2.0.0", VersionRange::parse("[1.0,2.0]")->toString().c_str());
        EXPECT_STREQ("1.0.0 < version < 2.0.0", VersionRange::parse("(1.0,2.0)")->toString().c_str());
        EXPECT_STREQ("1.0.0 <= version < 2.0.0", VersionRange::parse("[1.0,2.0)")->toString().c_str());
    }

} // namespace tinynuget::test

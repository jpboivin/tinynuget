//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/semantic_version.h>

#include <tuple>

#include <gtest/gtest.h>

namespace tinynuget::test
{
    TEST(TinyNuget_SemanticVersion, Parse_Valid)
    {
        for (auto& [versionStr, expectedVersion] : std::initializer_list<std::tuple<std::string_view, SemanticVersion>>{
                 {"0.0.4", SemanticVersion{0, 0, 4}},
                 {"1.2.3", SemanticVersion{1, 2, 3}},
                 {"10.20.30", SemanticVersion{10, 20, 30}},
                 {"1.1.2-prerelease+meta", SemanticVersion{1, 1, 2, "prerelease", "meta"}},
                 {"1.1.2+meta", SemanticVersion{1, 1, 2, std::nullopt, "meta"}},
                 {"1.1.2+meta-valid", SemanticVersion{1, 1, 2, std::nullopt, "meta-valid"}},
                 {"1.0.0-alpha", SemanticVersion{1, 0, 0, "alpha"}},
                 {"1.0.0-beta", SemanticVersion{1, 0, 0, "beta"}},
                 {"1.0.0-alpha.beta", SemanticVersion{1, 0, 0, "alpha.beta"}},
                 {"1.0.0-alpha.beta.1", SemanticVersion{1, 0, 0, "alpha.beta.1"}},
                 {"1.0.0-alpha.1", SemanticVersion{1, 0, 0, "alpha.1"}},
                 {"1.0.0-alpha0.valid", SemanticVersion{1, 0, 0, "alpha0.valid"}},
                 {"1.0.0-alpha.0valid", SemanticVersion{1, 0, 0, "alpha.0valid"}},
                 {"1.0.0-alpha-a.b-c-somethinglong+build.1-aef.1-its-okay",
                  SemanticVersion{1, 0, 0, "alpha-a.b-c-somethinglong", "build.1-aef.1-its-okay"}},
                 {"1.0.0-rc.1+build.1", SemanticVersion{1, 0, 0, "rc.1", "build.1"}},
                 {"2.0.0-rc.1+build.123", SemanticVersion{2, 0, 0, "rc.1", "build.123"}},
                 {"1.2.3-beta", SemanticVersion{1, 2, 3, "beta"}},
                 {"10.2.3-DEV-SNAPSHOT", SemanticVersion{10, 2, 3, "DEV-SNAPSHOT"}},
                 {"1.2.3-SNAPSHOT-123", SemanticVersion{1, 2, 3, "SNAPSHOT-123"}},
                 {"1.0.0", SemanticVersion{1, 0, 0}},
                 {"2.0.0", SemanticVersion{2, 0, 0}},
                 {"1.1.7", SemanticVersion{1, 1, 7}},
                 {"2.0.0+build.1848", SemanticVersion{2, 0, 0, std::nullopt, "build.1848"}},
                 {"2.0.1-alpha.1227", SemanticVersion{2, 0, 1, "alpha.1227"}},
                 {"1.0.0-alpha+beta", SemanticVersion{1, 0, 0, "alpha", "beta"}},
                 {"1.2.3----RC-SNAPSHOT.12.9.1--.12+788", SemanticVersion{1, 2, 3, "---RC-SNAPSHOT.12.9.1--.12", "788"}},
                 {"1.2.3----R-S.12.9.1--.12+meta", SemanticVersion{1, 2, 3, "---R-S.12.9.1--.12", "meta"}},
                 {"1.2.3----RC-SNAPSHOT.12.9.1--.12", SemanticVersion{1, 2, 3, "---RC-SNAPSHOT.12.9.1--.12"}},
                 {"1.0.0+0.build.1-rc.10000aaa-kk-0.1", SemanticVersion{1, 0, 0, std::nullopt, "0.build.1-rc.10000aaa-kk-0.1"}},
                 {"1.0.0-0A.is.legal", SemanticVersion{1, 0, 0, "0A.is.legal"}},
             })
        {
            auto version = SemanticVersion::parse(versionStr);
            ASSERT_TRUE(version) << "Valid SemVer: " << versionStr;
            EXPECT_EQ(expectedVersion, *version) << "Valid SemVer: " << versionStr;
        }
    }

    TEST(TinyNuget_SemanticVersion, Parse_Invalid)
    {
        for (auto versionStr : std::initializer_list<std::string_view>{
                 "",
                 "1",
                 "1.2",
                 "1.2.3-0123",
                 "1.2.3-0123.0123",
                 "1.1.2+.123",
                 "+invalid",
                 "-invalid",
                 "-invalid+invalid",
                 "-invalid.01",
                 "alpha",
                 "alpha.beta",
                 "alpha.beta.1",
                 "alpha.1",
                 "alpha+beta",
                 "alpha_beta",
                 "alpha.",
                 "alpha..",
                 "beta",
                 "1.0.0-alpha_beta",
                 "-alpha.",
                 "1.0.0-alpha..",
                 "1.0.0-alpha..1",
                 "1.0.0-alpha...1",
                 "1.0.0-alpha....1",
                 "1.0.0-alpha.....1",
                 "1.0.0-alpha......1",
                 "1.0.0-alpha.......1",
                 "01.1.1",
                 "1.01.1",
                 "1.1.01",
                 "1.2",
                 "1.2.3.DEV",
                 "1.2-SNAPSHOT",
                 "1.2.31.2.3----RC-SNAPSHOT.12.09.1--..12+788",
                 "1.2-RC-SNAPSHOT",
                 "-1.0.3-gamma+b7718",
                 "+justmeta",
                 "9.8.7+meta+meta",
                 "9.8.7-whatever+meta+meta",
                 "99999999999999999999999.999999999999999999.99999999999999999----RC-SNAPSHOT.12.09.1--------------------------------..12",
                 // this one is normally valid, but not with our implementation:
                 "99999999999999999999999.999999999999999999.99999999999999999",
             })
        {
            auto version = SemanticVersion::parse(versionStr);
            EXPECT_FALSE(version) << "Invalid SemVer: " << versionStr;
        }
    }

    TEST(TinyNuget_SemanticVersion, Ctor_Default)
    {
        SemanticVersion version = {};
        EXPECT_EQ(0U, version.major());
        EXPECT_EQ(0U, version.minor());
        EXPECT_EQ(0U, version.patch());
        EXPECT_FALSE(version.label());
        EXPECT_FALSE(version.metadata());
    }

    TEST(TinyNuget_SemanticVersion, Ctor_Major)
    {
        SemanticVersion version = {11};
        EXPECT_EQ(11U, version.major());
        EXPECT_EQ(0U, version.minor());
        EXPECT_EQ(0U, version.patch());
        EXPECT_FALSE(version.label());
        EXPECT_FALSE(version.metadata());
    }

    TEST(TinyNuget_SemanticVersion, Ctor_MajorMinor)
    {
        SemanticVersion version = {11, 22};
        EXPECT_EQ(11U, version.major());
        EXPECT_EQ(22U, version.minor());
        EXPECT_EQ(0U, version.patch());
        EXPECT_FALSE(version.label());
        EXPECT_FALSE(version.metadata());
    }

    TEST(TinyNuget_SemanticVersion, Ctor_MajorMinorPatch)
    {
        SemanticVersion version = {11, 22, 33};
        EXPECT_EQ(11U, version.major());
        EXPECT_EQ(22U, version.minor());
        EXPECT_EQ(33U, version.patch());
        EXPECT_FALSE(version.label());
        EXPECT_FALSE(version.metadata());
    }

    TEST(TinyNuget_SemanticVersion, Ctor_MajorMinorPatchLabel)
    {
        SemanticVersion version = {11, 22, 33, "alpha"};
        EXPECT_EQ(11U, version.major());
        EXPECT_EQ(22U, version.minor());
        EXPECT_EQ(33U, version.patch());
        ASSERT_TRUE(version.label());
        EXPECT_STREQ("alpha", version.label()->c_str());
        EXPECT_FALSE(version.metadata());
    }

    TEST(TinyNuget_SemanticVersion, Ctor_MajorMinorPatchLabelMetadata)
    {
        SemanticVersion version = {11, 22, 33, "alpha", "1afb34"};
        EXPECT_EQ(11U, version.major());
        EXPECT_EQ(22U, version.minor());
        EXPECT_EQ(33U, version.patch());
        ASSERT_TRUE(version.label());
        EXPECT_STREQ("alpha", version.label()->c_str());
        ASSERT_TRUE(version.metadata());
        EXPECT_STREQ("1afb34", version.metadata()->c_str());
    }

} // namespace tinynuget::test

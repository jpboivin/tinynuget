//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "protocol/v3/service_index.h"

#include <algorithm>

#include <gtest/gtest.h>

#define EXPECT_CONTAINS_RESOURCE(expectedType, expectedUrl, resources)                        \
    EXPECT_TRUE(std::find_if(resources.cbegin(), resources.cend(), [](const auto& resource) { \
                    return resource.type == expectedType && resource.url == expectedUrl;      \
                }) != resources.cend())

namespace tinynuget::test
{
    TEST(TinyNuget_Protocol_V3_ServiceIndex, FetchResources)
    {
        protocol::v3::ServiceIndex service{"https://api.nuget.org/v3/"};

        auto resources = service.fetchResources().get();
        EXPECT_FALSE(resources.empty());
        EXPECT_CONTAINS_RESOURCE("PackageBaseAddress/3.0.0", "https://api.nuget.org/v3-flatcontainer/", resources);
        EXPECT_CONTAINS_RESOURCE("Catalog/3.0.0", "https://api.nuget.org/v3/catalog0/index.json", resources);
    }

} // namespace tinynuget::test

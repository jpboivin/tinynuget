//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "protocol/v3/package_service.h"

#include <algorithm>

#include <gtest/gtest.h>

#define EXPECT_CONTAINS_VERSION(expectedVersion, versions)                                 \
    EXPECT_TRUE(std::find_if(versions.cbegin(), versions.cend(), [](const auto& version) { \
                    return version.normalize() == expectedVersion;                         \
                }) != versions.cend())

namespace tinynuget::test
{
    TEST(TinyNuget_Protocol_V3_PackageService, FetchVersions)
    {
        protocol::v3::PackageService service{"https://api.nuget.org/v3-flatcontainer/"};

        auto versions = service.fetchVersions("Newtonsoft.Json").get();
        EXPECT_FALSE(versions.empty());
        EXPECT_CONTAINS_VERSION("10.0.1", versions);
        EXPECT_CONTAINS_VERSION("11.0.1", versions);
        // EXPECT_CONTAINS_VERSION("11.0.1-beta3", versions); TODO fix SemanticVersion
    }

    TEST(TinyNuget_Protocol_V3_PackageService, FetchInfo)
    {
        protocol::v3::PackageService service{"https://api.nuget.org/v3-flatcontainer/"};

        auto package = service.fetchInfo("Newtonsoft.Json", *SemanticVersion::parse("10.0.1")).get();
        EXPECT_STREQ("Newtonsoft.Json", std::string{package.name()}.c_str());
        EXPECT_STREQ("10.0.1", package.version().normalize().c_str());
    }

} // namespace tinynuget::test

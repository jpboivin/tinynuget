//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/client.h"
#include "json/json.h"

#include <chrono>
#include <initializer_list>
#include <string_view>
#include <tuple>

#include <gtest/gtest.h>

using namespace std::chrono_literals;
using namespace std::string_view_literals;

#if defined(__APPLE__)
#    define TINYNUGET_USE_NSURLSESSION
#endif

#if defined(__linux__)
#    define TINYNUGET_USE_CURL
#endif

#if defined(_WIN32)
#    define TINYNUGET_USE_WINHTTP
#endif

namespace tinynuget::test
{
    TEST(TinyNuget_HTTP_Client, BaseAddress)
    {
        for (const auto& baseAddress : {"https://www.nuget.org/api/v2", "https://www.nuget.org/api/v2/"})
        {
            http::Client httpClient{baseAddress};
            EXPECT_STREQ("https://www.nuget.org/api/v2/", httpClient.baseAddress().c_str());
        }
    }

    TEST(TinyNuget_HTTP_Client, Fetch)
    {
        for (const auto& [baseAddress, resourceUrl] : std::initializer_list<std::tuple<std::string_view, std::string_view>>{
                 {"https://postman-echo.com", "get?foo1=bar1&foo2=bar2"},
                 {"https://postman-echo.com", "/get?foo1=bar1&foo2=bar2"},
                 {"https://postman-echo.com/", "get?foo1=bar1&foo2=bar2"},
                 {"https://postman-echo.com/", "/get?foo1=bar1&foo2=bar2"},
             })
        {
            http::Client httpClient{baseAddress};

            auto fetchTask = httpClient.fetch<JSON>(resourceUrl);
            fetchTask.wait_for(30s);

            auto response = fetchTask.get();

            // make sure we called the proper URL
            EXPECT_STREQ("https://postman-echo.com/get?foo1=bar1&foo2=bar2", response["url"].string_value().c_str());

            // make sure we requested JSON data
            EXPECT_STREQ("application/json", response["headers"]["accept"].string_value().c_str());
        }
    }

    TEST(TinyNuget_HTTP_Client, Fetch_InvalidDomain)
    {
        http::Client httpClient{"https://dummy.domain/"};

        auto fetchTask = httpClient.fetch<JSON>("foobar");
        fetchTask.wait_for(30s);

        try
        {
            fetchTask.get();
            GTEST_FAIL() << "get() did not throw HttpStatusException as expected.";
        }
#if defined(TINYNUGET_USE_NSURLSESSION)
        catch (NSErrorException& exc)
        {
            EXPECT_EQ(-1003, exc.code);
            EXPECT_STREQ("NSURLErrorDomain", exc.domain.c_str());
        }
#elif defined(TINYNUGET_USE_WINHTTP)
        catch (WinHttpException& exc)
        {
            EXPECT_EQ(12007U, exc.code); // ERROR_WINHTTP_NAME_NOT_RESOLVED
        }
#elif defined(TINYNUGET_USE_CURL)
        catch (CurlException& exc)
        {
            EXPECT_EQ(6, exc.code); // CURLE_COULDNT_RESOLVE_HOST
        }
#else
#    error Unsupported HTTP implementation
#endif
    }

    TEST(TinyNuget_HTTP_Client, Fetch_NonSuccessStatusCode)
    {
        http::Client httpClient{"https://postman-echo.com/"};

        auto fetchTask = httpClient.fetch<JSON>("not_found");
        fetchTask.wait_for(30s);

        try
        {
            fetchTask.get();
            GTEST_FAIL() << "get() did not throw HttpStatusException as expected.";
        }
        catch (HttpStatusException& exc)
        {
            EXPECT_EQ(404U, exc.statusCode);
        }
    }

    TEST(TinyNuget_HTTP_Client, Fetch_InvalidJSON)
    {
        http::Client httpClient{"https://www.google.com/"};

        auto fetchTask = httpClient.fetch<JSON>("/");
        fetchTask.wait_for(30s);

        try
        {
            fetchTask.get();
            GTEST_FAIL() << "get() did not throw HttpStatusException as expected.";
        }
        catch (JsonFormatException&)
        {
            // don't care, it depends on the parser
        }
    }

} // namespace tinynuget::test

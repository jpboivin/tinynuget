//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/client.h>
#include <tinynuget/exceptions.h>
#include <tinynuget/feed.h>

#include <chrono>

#include <gtest/gtest.h>

using namespace std::chrono_literals;

namespace tinynuget::test
{
    TEST(TinyNuget_Client, OpenFeed_UnsupportedProtocolVersion)
    {
        Client client;

        auto openTask = client.openFeed(default_feed_address, static_cast<ProtocolVersion>(1));
        openTask.wait_for(30s);

        try
        {
            openTask.get();
            GTEST_FAIL() << "get() did not throw UnsupportedProtocolException as expected.";
        }
        catch (UnsupportedProtocolException& exc)
        {
            EXPECT_EQ(static_cast<ProtocolVersion>(1), exc.version());
        }
    }

    TEST(TinyNuget_Client, OpenFeed_UnsupportedFeed)
    {
        Client client;

        auto openTask = client.openFeed("https://google.com/", ProtocolVersion::any);
        openTask.wait_for(30s);

        try
        {
            openTask.get();
            GTEST_FAIL() << "get() did not throw UnsupportedProtocolException as expected.";
        }
        catch (UnsupportedFeedException& exc)
        {
            EXPECT_STREQ("https://google.com/", std::string{exc.address()}.c_str());
        }
    }

    TEST(TinyNuget_Client, OpenFeed_V2_WithProtocolDetection)
    {
        Client client;

        auto openTask = client.openFeed(default_v2_feed_address, ProtocolVersion::any);
        openTask.wait_for(30s);

        [[maybe_unused]] auto feed = openTask.get();
        // discard the feed, it got opened -- suppose it is fine
    }

    TEST(TinyNuget_Client, OpenFeed_V2)
    {
        Client client;

        auto openTask = client.openFeed(default_v2_feed_address, ProtocolVersion::v2);
        openTask.wait_for(30s);

        [[maybe_unused]] auto feed = openTask.get();
        // discard the feed, it got opened -- suppose it is fine
    }

    TEST(TinyNuget_Client, OpenFeed_V3)
    {
        Client client;

        auto openTask = client.openFeed(default_v3_feed_address, ProtocolVersion::v3);
        openTask.wait_for(30s);

        [[maybe_unused]] auto feed = openTask.get();
        // discard the feed, it got opened -- suppose it is fine
    }

    TEST(TinyNuget_Client, OpenFeed_V3_WithProtocolDetection)
    {
        Client client;

        auto openTask = client.openFeed(default_v3_feed_address, ProtocolVersion::any);
        openTask.wait_for(30s);

        [[maybe_unused]] auto feed = openTask.get();
        // discard the feed, it got opened -- suppose it is fine
    }

} // namespace tinynuget::test

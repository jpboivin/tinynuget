//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/sha512.h"

#include <cstring>
#include <initializer_list>
#include <tuple>

#include <gtest/gtest.h>

namespace tinynuget::test
{
    TEST(TinyNuget_Utilities_SHA512, DigestSize)
    {
        EXPECT_EQ(512U, hash::SHA512::digest_size * 8U);
    }

    TEST(TinyNuget_Utilities_SHA512, MultipleUpdateAndFinalize)
    {
        // test vectors from NIST:
        // https://www.di-mgt.com.au/sha_testvectors.html
        for (const auto& [dataString, expectedHexDigest] : std::initializer_list<std::tuple<const char*, const char*>>{
                 {"abc",
                  "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f"},
                 {"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
                  "204a8fc6dda82f0a0ced7beb8e08a41657c16ef468b228a8279be331a703c33596fd15c13b1b07f9aa1d3bea57789ca031ad85c7a71dd70354ec631238ca3445"},
                 {"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu",
                  "8e959b75dae313da8cf4f72814fc143f8f7779c6eb9f7fa17299aeadb6889018501d289e4900f7e4331b99dec4b5433ac7d329eeb6dd26545e96e55b874be909"},
             })
        {
            uint8_t digest[hash::SHA512::digest_size];

            hash::SHA512 hash;
            for (size_t i = 0, len = std::strlen(dataString); i < len; ++i)
                hash.update(&dataString[i], sizeof(char));
            hash.finalize(digest);

            char hexDigest[hash::SHA512::digest_size * 2 + 1];
            for (size_t i = 0; i < hash::SHA512::digest_size; ++i)
                std::sprintf(hexDigest + (i * 2), "%02x", digest[i]);

            EXPECT_STREQ(expectedHexDigest, hexDigest);
        }
    }

    TEST(TinyNuget_Utilities_SHA512, SingleUpdateAndFinalize)
    {
        // test vectors from NIST:
        // https://www.di-mgt.com.au/sha_testvectors.html
        for (const auto& [dataString, expectedHexDigest] : std::initializer_list<std::tuple<const char*, const char*>>{
                 {"",
                  "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0ff8318d2877eec2f63b931bd47417a81a538327af927da3e"},
                 {"abc",
                  "ddaf35a193617abacc417349ae20413112e6fa4e89a97ea20a9eeee64b55d39a2192992a274fc1a836ba3c23a3feebbd454d4423643ce80e2a9ac94fa54ca49f"},
                 {"abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq",
                  "204a8fc6dda82f0a0ced7beb8e08a41657c16ef468b228a8279be331a703c33596fd15c13b1b07f9aa1d3bea57789ca031ad85c7a71dd70354ec631238ca3445"},
                 {"abcdefghbcdefghicdefghijdefghijkefghijklfghijklmghijklmnhijklmnoijklmnopjklmnopqklmnopqrlmnopqrsmnopqrstnopqrstu",
                  "8e959b75dae313da8cf4f72814fc143f8f7779c6eb9f7fa17299aeadb6889018501d289e4900f7e4331b99dec4b5433ac7d329eeb6dd26545e96e55b874be909"},
             })
        {
            uint8_t digest[hash::SHA512::digest_size];

            hash::SHA512 hash;
            hash.update(dataString, std::strlen(dataString));
            hash.finalize(digest);

            char hexDigest[hash::SHA512::digest_size * 2 + 1];
            for (size_t i = 0; i < hash::SHA512::digest_size; ++i)
                std::sprintf(hexDigest + (i * 2), "%02x", digest[i]);

            EXPECT_STREQ(expectedHexDigest, hexDigest);
        }
    }

} // namespace tinynuget::test

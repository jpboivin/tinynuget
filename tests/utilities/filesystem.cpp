//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/filesystem.h"

#include <cstdio>
#include <optional>
#include <set>
#include <string>

#if defined(__APPLE__) || defined(__linux__)
#    include <pwd.h>
#    include <unistd.h>
#endif

#if defined(_WIN32)
#    include <windows.h>
#endif

#include <gtest/gtest.h>

namespace tinynuget::test
{
    TEST(TinyNuget_Utilities_Filesystem, Normalize)
    {
        EXPECT_STREQ("C:/Program Files/Dummy/", filesystem::normalize("C:\\Program Files\\Dummy\\").c_str());
        EXPECT_STREQ("C:/Program Files/Dummy/", filesystem::normalize("C:/Program Files\\Dummy/").c_str());
        EXPECT_STREQ("/usr/bin/dummy", filesystem::normalize("/usr/bin/dummy").c_str());
    }

    TEST(TinyNuget_Utilities_Filesystem, Combine)
    {
        EXPECT_STREQ("C:/Program Files/Dummy/", filesystem::combine("C:\\Program Files", "Dummy/").c_str());
        EXPECT_STREQ("C:/Program Files/Dummy", filesystem::combine("C:/Program Files/", "/Dummy").c_str());
        EXPECT_STREQ("C:/Program Files/Dummy", filesystem::combine("C:\\Program Files\\", "\\Dummy").c_str());
        EXPECT_STREQ("C:/Program Files/Dummy", filesystem::combine("C:\\Program Files\\", "/Dummy").c_str());
        EXPECT_STREQ("C:/Program Files/Dummy/Program.exe", filesystem::combine("C:/Program Files", "Dummy", "Program.exe").c_str());
        EXPECT_STREQ("file.txt", filesystem::combine("", "file.txt").c_str());
    }

    TEST(TinyNuget_Utilities_Filesystem, Parent)
    {
        EXPECT_STREQ("C:/Program Files", filesystem::parent("C:/Program Files/Dummy").c_str());
        EXPECT_STREQ("C:/Program Files", filesystem::parent("C:/Program Files/Dummy/").c_str());
        EXPECT_STREQ("C:/Program Files/Foo", filesystem::parent("C:\\Program Files\\Foo\\Bar").c_str());
        EXPECT_STREQ("C:/Program Files/Foo", filesystem::parent("C:\\Program Files\\Foo\\Bar\\").c_str());
        EXPECT_STREQ("/usr/bin", filesystem::parent("/usr/bin/dummy").c_str());
    }

    TEST(TinyNuget_Utilities_Filesystem, Exists)
    {
        // cleanup (in case they still exist)
        filesystem::remove("dummy.txt");
        filesystem::remove("dummy2");

        EXPECT_TRUE(filesystem::exists("."));
        EXPECT_TRUE(filesystem::exists(".."));

        EXPECT_FALSE(filesystem::exists("dummy.txt"));
        FILE* file = std::fopen("dummy.txt", "wb");
        if (file != nullptr)
            std::fclose(file);
        EXPECT_TRUE(filesystem::exists("dummy.txt"));

        EXPECT_FALSE(filesystem::exists("dummy2"));
        filesystem::ensureDirectoryExists("dummy2");
        EXPECT_TRUE(filesystem::exists("dummy2"));
        EXPECT_TRUE(filesystem::exists("dummy2/"));
    }

    TEST(TinyNuget_Utilities_Filesystem, EnsureDirectoryExists)
    {
        // cleanup directories (in case they still exist)
        filesystem::remove("foo/bar/baz/for/test.txt");
        filesystem::remove("foo/bar/baz/for");
        filesystem::remove("foo/bar/baz");
        filesystem::remove("foo/bar");
        filesystem::remove("foo");

        // create multiple directories at once
        EXPECT_FALSE(filesystem::exists("foo/bar/baz"));
        filesystem::ensureDirectoryExists("foo/bar/baz");
        EXPECT_TRUE(filesystem::exists("foo/bar/baz"));

        // create one directory
        EXPECT_FALSE(filesystem::exists("foo/bar/baz/for/"));
        filesystem::ensureDirectoryExists("foo/bar/baz/for/");
        EXPECT_TRUE(filesystem::exists("foo/bar/baz/for"));

        // ensure the full path exist -- we should be able to create a file there
        FILE* file = std::fopen("foo/bar/baz/for/test.txt", "wb");
        if (file != nullptr)
            std::fclose(file);
        EXPECT_TRUE(filesystem::exists("foo/bar/baz/for/test.txt"));
    }

    TEST(TinyNuget_Utilities_Filesystem, Remove_File)
    {
        EXPECT_FALSE(filesystem::remove("non_existing_file.txt"));


        FILE* file = std::fopen("existing_file.txt", "wb");
        if (file != nullptr)
            std::fclose(file);

        EXPECT_TRUE(filesystem::exists("existing_file.txt"));
        EXPECT_TRUE(filesystem::remove("existing_file.txt"));
        EXPECT_FALSE(filesystem::exists("existing_file.txt"));
    }

    TEST(TinyNuget_Utilities_Filesystem, Remove_Directory)
    {
        EXPECT_FALSE(filesystem::remove("non_existing_folder/"));


        filesystem::ensureDirectoryExists("existing_folder/");

        EXPECT_TRUE(filesystem::exists("existing_folder/"));
        EXPECT_TRUE(filesystem::remove("existing_folder/"));
        EXPECT_FALSE(filesystem::exists("existing_folder/"));


        filesystem::ensureDirectoryExists("non_empty_folder/");
        FILE* file = std::fopen("non_empty_folder/dummy.txt", "wb");
        if (file != nullptr)
            std::fclose(file);

        EXPECT_TRUE(filesystem::exists("non_empty_folder/"));
        EXPECT_FALSE(filesystem::remove("non_empty_folder/"));
        EXPECT_TRUE(filesystem::exists("non_empty_folder/"));
    }

    TEST(TinyNuget_Utilities_Filesystem, RemoveAll_File)
    {
        EXPECT_FALSE(filesystem::removeAll("non_existing_file.txt"));


        FILE* file = std::fopen("existing_file.txt", "wb");
        if (file != nullptr)
            std::fclose(file);

        EXPECT_TRUE(filesystem::exists("existing_file.txt"));
        EXPECT_TRUE(filesystem::removeAll("existing_file.txt"));
        EXPECT_FALSE(filesystem::exists("existing_file.txt"));
    }

    TEST(TinyNuget_Utilities_Filesystem, RemoveAll_Directory)
    {
        EXPECT_FALSE(filesystem::remove("non_existing_folder/"));


        filesystem::ensureDirectoryExists("existing_folder/");

        EXPECT_TRUE(filesystem::exists("existing_folder/"));
        EXPECT_TRUE(filesystem::removeAll("existing_folder/"));
        EXPECT_FALSE(filesystem::exists("existing_folder/"));


        filesystem::ensureDirectoryExists("non_empty_folder/foo/bar/");
        FILE* file = std::fopen("non_empty_folder/dummy.txt", "wb");
        if (file != nullptr)
            std::fclose(file);

        EXPECT_TRUE(filesystem::exists("non_empty_folder/"));
        EXPECT_TRUE(filesystem::removeAll("non_empty_folder/"));
        EXPECT_FALSE(filesystem::exists("non_empty_folder/"));
    }


#if defined(__APPLE__) || defined(__linux__)
#    define TINYNUGET_SET_ENVIRONMENT_VARIABLE(name, value) setenv((name), (value), 1)
#    define TINYNUGET_UNSET_ENVIRONMENT_VARIABLE(name)      unsetenv((name))
#elif defined(_WIN32)
#    define TINYNUGET_SET_ENVIRONMENT_VARIABLE(name, value) SetEnvironmentVariableA((name), (value))
#    define TINYNUGET_UNSET_ENVIRONMENT_VARIABLE(name)      SetEnvironmentVariableA((name), NULL)
#endif

    class TinyNuget_Utilities_Filesystem_NuGet : public ::testing::Test
    {
    public:
        static constexpr char home_directory_env_name[] = "HOME";
        static constexpr char packages_directory_env_name[] = "NUGET_PACKAGES";

        void SetUp() override
        {
            char* variable = nullptr;

            variable = getenv(home_directory_env_name);
            m_originalHomeDirectory = variable != nullptr ? std::optional<std::string>{variable} : std::nullopt;

            variable = getenv(packages_directory_env_name);
            m_originalPackagesDirectory = variable != nullptr ? std::optional<std::string>{variable} : std::nullopt;

            TINYNUGET_UNSET_ENVIRONMENT_VARIABLE(home_directory_env_name);
            TINYNUGET_UNSET_ENVIRONMENT_VARIABLE(packages_directory_env_name);
        }

        void TearDown() override
        {
            if (m_originalHomeDirectory)
            {
                TINYNUGET_SET_ENVIRONMENT_VARIABLE(home_directory_env_name, m_originalHomeDirectory->c_str());
            }

            if (m_originalPackagesDirectory)
            {
                TINYNUGET_SET_ENVIRONMENT_VARIABLE(packages_directory_env_name, m_originalPackagesDirectory->c_str());
            }
        }

    protected:
        std::string currentUser() const
        {
#if defined(__APPLE__) || defined(__linux__)
            return getpwuid(getuid())->pw_name;
#elif defined(_WIN32)
            char  username[256];
            DWORD usernameSize = sizeof(username);
            GetUserNameA(username, &usernameSize);

            return username;
#else
            return "unknown";
#endif
        }

        std::string currentUserDirectory()
        {
            const auto user = currentUser();

#if defined(__APPLE__)
            return filesystem::combine("/Users", user);
#elif defined(__linux__)
            return user != "root" ? filesystem::combine("/home", user) : "/root";
#elif defined(_WIN32)
            return filesystem::combine("C:/Users", user);
#endif
        }

    private:
        std::optional<std::string> m_originalHomeDirectory;
        std::optional<std::string> m_originalPackagesDirectory;
    };

    TEST_F(TinyNuget_Utilities_Filesystem_NuGet, GlobalDirectory_Default)
    {
        auto expectedGlobalDirectory = currentUserDirectory() + "/.nuget/packages";
        auto globalDirectory = filesystem::nuget::globalDirectory();
        EXPECT_STREQ(expectedGlobalDirectory.c_str(), globalDirectory.c_str());
    }

#if !defined(_WIN32)
    TEST_F(TinyNuget_Utilities_Filesystem_NuGet, GlobalDirectory_CustomHome)
    {
        TINYNUGET_SET_ENVIRONMENT_VARIABLE(home_directory_env_name, "/some/user/folder");

        auto globalDirectory = filesystem::nuget::globalDirectory();
        EXPECT_STREQ("/some/user/folder/.nuget/packages", globalDirectory.c_str());
    }
#endif

    TEST_F(TinyNuget_Utilities_Filesystem_NuGet, GlobalDirectory_CustomPath)
    {
        TINYNUGET_SET_ENVIRONMENT_VARIABLE(packages_directory_env_name, "/some/specific/folder");

        auto globalDirectory = filesystem::nuget::globalDirectory();
        EXPECT_STREQ("/some/specific/folder", globalDirectory.c_str());
    }

    TEST_F(TinyNuget_Utilities_Filesystem_NuGet, ScratchDirectory_Default)
    {
#if defined(_WIN32)
        // TEMP always returns a short file name for backward compatibility
        // https://devblogs.microsoft.com/oldnewthing/20041224-00/?p=36893
        char shortCurrentUserDirectory[MAX_PATH] = {};
        GetShortPathNameA(currentUserDirectory().c_str(), shortCurrentUserDirectory, sizeof(shortCurrentUserDirectory));

        auto expectedScratchDirectory = filesystem::combine(shortCurrentUserDirectory, "/AppData/Local/Temp/NuGetScratch");
        auto scratchDirectory = filesystem::nuget::scratchDirectory();
        EXPECT_STREQ(expectedScratchDirectory.c_str(), scratchDirectory.c_str());
#else
        auto scratchDirectory = filesystem::nuget::scratchDirectory();
        EXPECT_STREQ("/tmp/NuGetScratch", scratchDirectory.c_str());
#endif
    }

    TEST_F(TinyNuget_Utilities_Filesystem_NuGet, TempDirectory_Default)
    {
        auto scratchDirectory = filesystem::nuget::scratchDirectory();
        auto tempDirectory = filesystem::nuget::tempDirectory();

        ASSERT_GT(tempDirectory.size(), scratchDirectory.size());
        EXPECT_STREQ(scratchDirectory.c_str(), tempDirectory.substr(0, scratchDirectory.size()).c_str());

        std::set<std::string> tempDirectories;
        for (int i = 0; i < 50; ++i)
        {
            tempDirectory = filesystem::nuget::tempDirectory();
            auto ret = tempDirectories.insert(tempDirectory);
            EXPECT_TRUE(ret.second);
        }
    }

} // namespace tinynuget::test

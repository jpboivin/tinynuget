//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/base64.h"

#include <cstring>
#include <initializer_list>
#include <tuple>

#include <gtest/gtest.h>

namespace tinynuget::test
{
    TEST(TinyNuget_Utilities_Base64, Encode)
    {
        // test vectors from RFC:
        // https://tools.ietf.org/html/rfc4648
        for (const auto& [dataString, expectedBase64String] : std::initializer_list<std::tuple<const char*, const char*>>{
                 {"", ""},
                 {"f", "Zg=="},
                 {"fo", "Zm8="},
                 {"foo", "Zm9v"},
                 {"foob", "Zm9vYg=="},
                 {"fooba", "Zm9vYmE="},
                 {"foobar", "Zm9vYmFy"},
             })
        {
            EXPECT_STREQ(expectedBase64String, base64::encode(dataString, std::strlen(dataString)).c_str());
        }
    }

} // namespace tinynuget::test

#
# Copyright (C) 2020 Jean-Philippe Boivin. All rights reserved.
#

set(CMAKE_POLICY_DEFAULT_CMP0069 NEW)

if (TINYNUGET_ENABLE_V2_PROTOCOL)
    set(BUILD_STATIC_LIBS ON CACHE BOOL "Enable tinyxml2 static library.")
    set(BUILD_SHARED_LIBS OFF CACHE BOOL "Disable tinyxml2 dynamic library.")
    set(BUILD_TESTS OFF CACHE BOOL "Disable tinyxml2 tests.")
    set(BUILD_TESTING OFF CACHE BOOL "Disable tinyxml2 tests.")

    add_subdirectory(tinyxml2)
    set_target_properties(tinyxml2 PROPERTIES FOLDER "Externals")
endif()

if (TINYNUGET_ENABLE_V3_PROTOCOL)
    add_subdirectory(json11)
    set_target_properties(json11 PROPERTIES FOLDER "Externals")

    if (TINYNUGET_PLATFORM_WINDOWS)
        # json11 improperly set compile options for MSVC -- just reset them
        set_target_properties(json11 PROPERTIES COMPILE_OPTIONS "")
    endif()
endif()

if (TINYNUGET_ENABLE_MINIZIP)
    set(MZ_COMPAT           OFF CACHE BOOL "Enables compatibility layer.")
    set(MZ_ZLIB             ON  CACHE BOOL "Enables ZLIB compression")
    set(MZ_BZIP2            OFF CACHE BOOL "Enables BZIP2 compression.")
    set(MZ_LZMA             OFF CACHE BOOL "Enables LZMA compression.")
    set(MZ_ZSTD             OFF CACHE BOOL "Enables ZSTD compression.")
    set(MZ_PKCRYPT          OFF CACHE BOOL "Enables PKWARE traditional encryption.")
    set(MZ_WZAES            OFF CACHE BOOL "Enables WinZIP AES encryption.")
    set(MZ_LIBCOMP          OFF CACHE BOOL "Enables Apple compression.") # Note. This one could be ON on Darwin
    set(MZ_OPENSSL          OFF CACHE BOOL "Enables OpenSSL encryption.")
    set(MZ_LIBBSD           OFF CACHE BOOL "Builds with libbsd crypto random")
    set(MZ_BRG              OFF CACHE BOOL "Enables Brian Gladman's library.")
    set(MZ_SIGNING          OFF CACHE BOOL "Enables zip signing support.")
    set(MZ_COMPRESS_ONLY    OFF CACHE BOOL "Only support compression.")
    set(MZ_DECOMPRESS_ONLY  ON  CACHE BOOL "Only support decompression")
    set(MZ_BUILD_TEST       OFF CACHE BOOL "Builds minizip test executable.")
    set(MZ_BUILD_UNIT_TEST  OFF CACHE BOOL "Builds minizip unit test project.")
    set(MZ_BUILD_FUZZ_TEST  OFF CACHE BOOL "Builds minizip fuzz executables.")
    set(MZ_CODE_COVERAGE    OFF CACHE BOOL "Builds with code coverage flags.")
    set(MZ_FILE32_API       OFF CACHE BOOL "Builds using posix 32-bit file api")

    add_subdirectory(minizip)
    set_target_properties(minizip PROPERTIES FOLDER "Externals")
endif()

if (TRUE)
    set(BUILD_TESTING OFF CACHE BOOL "Disable TinyProcessLibrary tests.")

    add_subdirectory(tiny-process-library)
    set_target_properties(tiny-process-library PROPERTIES FOLDER "Externals")
endif()

if (TINYNUGET_BUILD_TESTS)
    set(BUILD_GMOCK OFF CACHE BOOL "Disable googlemock in this project.")
    set(INSTALL_GTEST OFF CACHE BOOL "Disable googletest installation in this project.")
    set(gtest_force_shared_crt ON CACHE BOOL "Force googletest to use the MD runtime.")

    add_subdirectory(googletest)
    set_target_properties(gtest PROPERTIES FOLDER "Externals/Tests")
    set_target_properties(gtest_main PROPERTIES FOLDER "Externals/Tests")
endif()

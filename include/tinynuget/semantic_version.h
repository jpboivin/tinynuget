//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the organization nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TINYNUGET_SEMANTIC_VERSION_H
#define TINYNUGET_SEMANTIC_VERSION_H

#include <cstdint>
#include <optional>
#include <string>
#include <string_view>

namespace tinynuget
{
    /**
     * @brief SemanticVersion represents a product version.
     * @see https://docs.microsoft.com/en-us/nuget/concepts/package-versioning#semantic-versioning-200
     * @see https://semver.org
     */
    class SemanticVersion final
    {
    public:
        /**
         * @brief Parse a semantic version.
         * @param[in] version A string representing a semantic version.
         * @return On success, the parsed semantic version; std::nullopt otherwise.
         */
        [[nodiscard]] static std::optional<SemanticVersion> parse(std::string_view version);

    public:
        SemanticVersion(uint8_t                                major = 0,
                        uint8_t                                minor = 0,
                        uint8_t                                patch = 0,
                        const std::optional<std::string_view>& label = std::nullopt,
                        const std::optional<std::string_view>& metadata = std::nullopt);
        ~SemanticVersion();

        /**
         * @brief Get the major version.
         * @return The major version.
         */
        [[nodiscard]] uint8_t major() const noexcept;

        /**
         * @brief Get the minor version.
         * @return The minor version.
         */
        [[nodiscard]] uint8_t minor() const noexcept;

        /**
         * @brief Get the patch version.
         * @return The patch version.
         */
        [[nodiscard]] uint8_t patch() const noexcept;

        /**
         * @brief Get the optional label.
         * @return The label, if any.
         */
        [[nodiscard]] const std::optional<std::string>& label() const noexcept;

        /**
         * @brief Get the optional build metadata.
         * @return The build metadata, if any.
         */
        [[nodiscard]] const std::optional<std::string>& metadata() const noexcept;

        /**
         * @brief Normalize the version as required by the NuGet protocol.
         * @return The normalized version.
         * @see https://docs.microsoft.com/en-us/nuget/concepts/package-versioning#normalized-version-numbers
         */
        [[nodiscard]] std::string normalize() const noexcept;

        /**
         * @brief Compare the version with other.
         * @param[in] other The other version to compare with.
         * @return A negative value if this version is ordered before other;
         *         Zero if this version and other are equal;
         *         A positive value if this version is ordered after other.
         * @remarks The build metadata is not considered when ordering semantic versions.
         */
        [[nodiscard]] int compare(const SemanticVersion& other) const noexcept;

    private:
        uint8_t                    m_major;
        uint8_t                    m_minor;
        uint8_t                    m_patch;
        std::optional<std::string> m_label;
        std::optional<std::string> m_metadata;
    };

    [[nodiscard]] bool operator==(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept;
    [[nodiscard]] bool operator!=(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept;

    [[nodiscard]] bool operator>(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept;
    [[nodiscard]] bool operator>=(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept;
    [[nodiscard]] bool operator<(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept;
    [[nodiscard]] bool operator<=(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept;

} // namespace tinynuget

#endif // TINYNUGET_SEMANTIC_VERSION_H

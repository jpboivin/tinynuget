//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the organization nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TINYNUGET_VERSION_RANGE_H
#define TINYNUGET_VERSION_RANGE_H

#include <tinynuget/semantic_version.h>

#include <optional>
#include <string>
#include <string_view>

namespace tinynuget
{
    class SemanticVersion;

    /**
     * @brief VersionRange represents a range of version.
     * @see https://docs.microsoft.com/en-us/nuget/concepts/package-versioning#version-ranges
     */
    class VersionRange
    {
    public:
        /**
         * @brief Parse a version range.
         * @param[in] range Either a floating version or a NuGet version range.
         * @return On success, the parsed version range; std::nullopt otherwise.
         */
        [[nodiscard]] static std::optional<VersionRange> parse(std::string_view range);

    public:
        /**
         * @brief Check whether a version is within the range.
         * @param[in] version The version to check for.
         * @return true if the version is within the range; false otherwise.
         */
        [[nodiscard]] bool in(const SemanticVersion& version) const noexcept;

        /**
         * @brief toString
         * @return
         */
        [[nodiscard]] std::string toString() const noexcept;

    private:
        using VersionComparator = bool (*)(const SemanticVersion&, const SemanticVersion&);

        VersionRange(std::optional<SemanticVersion> minimumVersion,
                     VersionComparator              minimumCompare,
                     std::optional<SemanticVersion> maximumVersion,
                     VersionComparator              maximumCompare);

    private:
        std::optional<SemanticVersion> m_minimumVersion;
        VersionComparator              m_minimumCompare;
        std::optional<SemanticVersion> m_maximumVersion;
        VersionComparator              m_maximumCompare;
    };

} // namespace tinynuget

#endif // TINYNUGET_VERSION_RANGE_H

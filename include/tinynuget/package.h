//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the organization nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TINYNUGET_PACKAGE_H
#define TINYNUGET_PACKAGE_H

#include <tinynuget/semantic_version.h>

#include <cstdint>
#include <future>
#include <map>
#include <optional>
#include <string>
#include <string_view>
#include <vector>

#include <stdio.h>

namespace tinynuget
{
    namespace protocol::v2
    {
        class PackageService;
    }

    namespace protocol::v3
    {
        class PackageService;
    }

    class Package
    {
    public:
#if defined(_MSVC_STL_UPDATE)
        // There is a known issue with std::future implementation in the v14x runtime. It requires a T with a default ctor.
        // It has been fixed in the ABI breaking branch, waiting for a future MSVC STL version to remove this.
        // https://developercommunity.visualstudio.com/content/problem/1122412/stdfuture-and-stdshared-future-require-t-to-have-d.html
        Package() = default;
#endif

        /**
         * @brief Get the name of the package.
         * @return The name of the package.
         */
        [[nodiscard]] std::string_view name() const;

        /**
         * @brief Get the version of the package.
         * @return The version of the package.
         */
        [[nodiscard]] SemanticVersion version() const;

        /**
         * @brief Check whether the package is globally installed.
         * @return true if the package is installed; false otherwise.
         */
        [[nodiscard]] bool installed() const;

        /**
         * @brief Check whether the package is installed in the specified directory.
         * @param[in] installDir  The directory in which the package should be installed.
         * @return true if the package is installed; false otherwise.
         */
        [[nodiscard]] bool installedIn(std::string_view installDir) const;

        /**
         * @brief Get the directory in which to find the package content, based on the global install directory.
         * @return The directory in which to find the package.
         */
        [[nodiscard]] std::string directory() const;

        /**
         * @brief Get the directory in which to find the package content, based on the specified install directory.
         * @param[in] installDir  The directory in which the package should be installed.
         * @return The directory in which to find the package.
         */
        [[nodiscard]] std::string directoryFor(std::string_view installDir) const;

        /**
         * @brief Install the package globally.
         * @remarks If the package is already installed, nothing will be done.
         */
        void install();

        /**
         * @brief Install the package in the specified directory.
         * @param[in] installDir  The directory in which the package should be installed.
         * @remarks The package will also be globally installed, as done by the regular NuGet client.
         */
        void installIn(std::string_view installDir);

    private:
        friend class protocol::v2::PackageService;
        friend class protocol::v3::PackageService;
        Package(std::string name, SemanticVersion version, std::string url, std::optional<std::string> hash = std::nullopt);

        /**
         * @brief computeHash
         * @param packagePath
         */
        void computeHash(const std::string& packagePath) const;

    private:
        std::string                m_name;
        SemanticVersion            m_version;
        std::string                m_url;
        std::optional<std::string> m_hash;
    };

} // namespace tinynuget

#endif // TINYNUGET_PACKAGE_H

//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the organization nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TINYNUGET_FEED_H
#define TINYNUGET_FEED_H

#include <future>
#include <map>
#include <string>
#include <string_view>

namespace tinynuget
{
    class Client;
    class Feed;
    class Package;
    class VersionRange;

    namespace protocol
    {
        namespace v2
        {
            std::future<Feed> openFeed(std::string_view);
        }

        namespace v3
        {
            std::future<Feed> openFeed(std::string_view);
        }

        class BasePackageService;
    } // namespace protocol

    /**
     * @brief The feed address for nuget.org (using the v2 protocol).
     */
    constexpr std::string_view default_v2_feed_address = "https://www.nuget.org/api/v2/";

    /**
     * @brief The feed address for nuget.org (using the v3 protocol).
     */
    constexpr std::string_view default_v3_feed_address = "https://api.nuget.org/v3/";

    /**
     * @brief The feed address for nuget.org.
     */
    constexpr std::string_view default_feed_address = default_v3_feed_address;

    class Feed final
    {
    public:
#if defined(_MSVC_STL_UPDATE)
        // There is a known issue with std::future implementation in the v14x runtime. It requires a T with a default ctor.
        // It has been fixed in the ABI breaking branch, waiting for a future MSVC STL version to remove this.
        // https://developercommunity.visualstudio.com/content/problem/1122412/stdfuture-and-stdshared-future-require-t-to-have-d.html
        Feed();
#endif
        ~Feed();

        Feed(const Feed& other) = delete;
        Feed(Feed&& other) noexcept;
        Feed& operator=(const Feed& other) = delete;
        Feed& operator=(Feed&& other) noexcept;

        /**
         * @brief Resolve a package.
         * @param[in] packageName  The name of the package.
         * @param[in] packageVersion  The version of the package (the latest matching version will be selected).
         * @return A future that will hold the resolved package; or an exception on failure.
         * @throws PackageNotFoundException The package is not found in the feed.
         * @throws VersionNotFoundException The package has no matching version in the feed.
         */
        [[nodiscard]] std::future<Package> resolvePackage(std::string_view packageName, const VersionRange& packageVersion);

    private:
        friend std::future<Feed> protocol::v2::openFeed(std::string_view);
        friend std::future<Feed> protocol::v3::openFeed(std::string_view);
        Feed(std::unique_ptr<protocol::BasePackageService> packageService);

    private:
        std::unique_ptr<protocol::BasePackageService> m_packageService;
    };

} // namespace tinynuget

#endif // TINYNUGET_FEED_H

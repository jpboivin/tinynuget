//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//  All rights reserved.
//
//  Redistribution and use in source and binary forms, with or without
//  modification, are permitted provided that the following conditions are met:
//
//     * Redistributions of source code must retain the above copyright
//       notice, this list of conditions and the following disclaimer.
//     * Redistributions in binary form must reproduce the above copyright
//       notice, this list of conditions and the following disclaimer in the
//       documentation and/or other materials provided with the distribution.
//     * Neither the name of the organization nor the names of its contributors
//       may be used to endorse or promote products derived from this software
//       without specific prior written permission.
//
//  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
//  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
//  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
//  DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
//  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
//  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
//  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
//  SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//

#ifndef TINYNUGET_EXCEPTIONS_H
#define TINYNUGET_EXCEPTIONS_H

#include <tinynuget/client.h>
#include <tinynuget/version_range.h>

#include <exception>
#include <string>
#include <string_view>

namespace tinynuget
{
    /**
     * @brief UnsupportedProtocolException is thrown by the library to indicate
     *        that the requested NuGet protocol is not supported.
     */
    class UnsupportedProtocolException final : public std::exception
    {
    public:
        UnsupportedProtocolException(ProtocolVersion version) noexcept;
        ~UnsupportedProtocolException() override = default;

        /**
         * @brief Get the requested NuGet protocol version.
         * @return The protocol version.
         */
        [[nodiscard]] ProtocolVersion version() const noexcept;

        /**
         * @brief Get an explanatory string.
         * @return A pointer to a NUL-terminated string with explanatory information.
         */
        [[nodiscard]] const char* what() const noexcept override;

    private:
        ProtocolVersion m_version;
    };

    /**
     * @brief UnsupportedFeedException is thrown by the library to indicate
     *        that the NuGet feed cannot be opened as it isn't supported.
     * @details A NuGet feed might not be supported for multiple reasons.
     *          It can be that the API is not recognized, or that some
     *          mandatory services are not available.
     */
    class UnsupportedFeedException final : public std::exception
    {
    public:
        UnsupportedFeedException(std::string address);
        ~UnsupportedFeedException() override = default;

        /**
         * @brief Get the address of the NuGet feed.
         * @return The address of the feed.
         */
        [[nodiscard]] std::string_view address() const noexcept;

        /**
         * @brief Get an explanatory string.
         * @return A pointer to a NUL-terminated string with explanatory information.
         */
        [[nodiscard]] const char* what() const noexcept override;

    private:
        std::string m_address;
    };

    /**
     * @brief PackageNotFoundException is thrown by the library to indicate
     *        that the requested NuGet package is not found in the feed.
     */
    class PackageNotFoundException final : public std::exception
    {
    public:
        PackageNotFoundException(std::string name);
        ~PackageNotFoundException() override = default;

        /**
         * @brief Get the name of the requested package.
         * @return The name of the package.
         */
        [[nodiscard]] std::string_view name() const noexcept;

        /**
         * @brief Get an explanatory string.
         * @return A pointer to a NUL-terminated string with explanatory information.
         */
        [[nodiscard]] const char* what() const noexcept override;

    private:
        std::string m_name;
    };

    /**
     * @brief VersionNotFoundException is thrown by the library to indicate
     *        that the requested NuGet package has no matching version in the feed.
     */
    class VersionNotFoundException final : public std::exception
    {
    public:
        VersionNotFoundException(std::string name, VersionRange range);
        ~VersionNotFoundException() override = default;

        /**
         * @brief Get the name of the requested package.
         * @return The name of the package.
         */
        [[nodiscard]] std::string_view name() const noexcept;

        /**
         * @brief Get the requested version range.
         * @return The version range of the package.
         */
        [[nodiscard]] const VersionRange& range() const noexcept;

        /**
         * @brief Get an explanatory string.
         * @return A pointer to a NUL-terminated string with explanatory information.
         */
        [[nodiscard]] const char* what() const noexcept override;

    private:
        std::string  m_name;
        VersionRange m_range;
    };

} // namespace tinynuget

#endif // TINYNUGET_EXCEPTIONS_H

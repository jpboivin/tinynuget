# protoc

`protoc` is an example of a proxy-executable relying on a NuGet package. Calling `protoc` will install the Protobuf tools and forward the arguments to the actual Protobuf compiler.

## Configurations

It is possible to configure the proxy by adding a `protoc.json` (or `protoc.exe.json` on Microsoft Windows) file next to the executable. The following parameters can be overriden:

- `feed_address`: Defines an alternative feed (by default, `nuget.org` will be used).
- `package_name`: Defines an alternative package (by default, `Google.Protobuf.Tools` will be used).
- `package_version`: Defines an alternative version (by default, `(,4.0.0]` will be used).
- `executable_path`: Defines an alternative path to the executable (by default, `tools/${platform}/protoc` will be used).
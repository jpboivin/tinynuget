//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/client.h>
#include <tinynuget/feed.h>
#include <tinynuget/package.h>
#include <tinynuget/version_range.h>

#include <cstdio>
#include <string>
#include <vector>

#include <json11.hpp>
#include <process.hpp>

#if defined(__APPLE__) || defined(__linux__)
#    include <sys/stat.h>
#    include <sys/types.h>
#    include <unistd.h>
#endif

static char default_feed_address[1024] = "https://api.nuget.org/v3/";
static char default_package_name[256] = "Google.Protobuf.Tools";
static char default_package_version_range[64] = "(,4.0.0]";
#if defined(__APPLE__)
static char default_executable_path[256] = "tools/macosx_x64/protoc";
#elif defined(__linux__)
static char default_executable_path[256] = "tools/linux_x64/protoc";
#elif defined(_WIN32)
static char default_executable_path[256] = "tools/windows_x64/protoc";
#endif

int main(int argc, const char* argv[])
{
    std::string feedAddress = default_feed_address;
    std::string packageName = default_package_name;
    std::string packageVersionRange = default_package_version_range;
    std::string relativeExecutablePath = default_executable_path;

    // parse the config file -- if present
    {
        std::string configFile = std::string{argv[0]} + ".json";
        std::string configContent;

        FILE* configStream = std::fopen(configFile.c_str(), "rt");
        if (configStream != nullptr)
        {
            char   buf[4096];
            size_t read;

            while ((read = std::fread(buf, sizeof(buf[0]), sizeof(buf) / sizeof(buf[0]), configStream)) != 0)
                configContent.append(buf, read);

            std::fclose(configStream);
        }

        if (!configContent.empty())
        {
            std::string  parsingErrors;
            json11::Json configJson = json11::Json::parse(configContent, parsingErrors, json11::JsonParse::COMMENTS);

            if (!parsingErrors.empty())
            {
                std::fprintf(stderr, "Failed to parse the configuration file.\nErrors: %s\n", parsingErrors.c_str());
                return EXIT_FAILURE;
            }

            if (!configJson["feed_address"].is_null())
                feedAddress = configJson["feed_address"].string_value();
            if (!configJson["package_name"].is_null())
                packageName = configJson["package_name"].string_value();
            if (!configJson["package_version"].is_null())
                packageVersionRange = configJson["package_version"].string_value();
            if (!configJson["executable_path"].is_null())
                relativeExecutablePath = configJson["executable_path"].string_value();
        }
    }

    try
    {
        tinynuget::Client  client;
        tinynuget::Feed    feed = client.openFeed(feedAddress).get();
        tinynuget::Package package = feed.resolvePackage(packageName, tinynuget::VersionRange::parse(packageVersionRange).value()).get();

        if (!package.installed())
            package.install();

        std::string executablePath = package.directory() + '/' + relativeExecutablePath;

#if defined(__APPLE__) || defined(__linux__)
        struct stat executableInfo;
        if (stat(executablePath.c_str(), &executableInfo) == 0)
        {
            if ((executableInfo.st_mode & S_IXUSR) == 0)
            {
                // Nuget packages are often created on Windows and don't have the proper +x permission for Unix-like systems
                chmod(executablePath.c_str(), executableInfo.st_mode | S_IXUSR);
            }
        }
#endif

        std::vector<std::string> executableArgs;
        executableArgs.emplace_back(executablePath);
        for (int i = 1; i < argc; ++i)
            executableArgs.emplace_back(argv[i]);

        constexpr auto redirectStdout = [](const char* str, const size_t size) {
            std::fprintf(stdout, "%s", std::string{str, size}.c_str());
            std::fflush(stdout);
        };
        constexpr auto redirectStderr = [](const char* str, const size_t size) {
            std::fprintf(stderr, "%s", std::string{str, size}.c_str());
            std::fflush(stderr);
        };

        TinyProcessLib::Process process{executableArgs, "", redirectStdout, redirectStderr, false};
        return process.get_exit_status();
    }
    catch (std::exception& exc)
    {
        std::fprintf(stderr, "Unhandled exception.\nException: %s\n", exc.what());
        std::fflush(stderr);
        return EXIT_FAILURE;
    }
}

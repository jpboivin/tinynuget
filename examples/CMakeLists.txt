#
# Copyright (C) 2020 Jean-Philippe Boivin. All rights reserved.
#

add_subdirectory(nuget_installer)
add_subdirectory(protoc)
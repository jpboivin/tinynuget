# nuget-installer

`nuget-installer` is an example of simplistic package installer.

## Usage

`nuget-installer <packageName> <packageVersion> [<packageDestination>]`
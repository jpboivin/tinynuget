//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/client.h>
#include <tinynuget/feed.h>
#include <tinynuget/package.h>
#include <tinynuget/version_range.h>

#include <iostream>
#include <optional>
#include <string>

int main(int argc, const char* argv[])
{
    std::string                packageName;
    std::string                packageVersionRangeStr;
    std::optional<std::string> packageDestination;

    if (argc <= 2)
    {
        std::cerr << "Usage: nuget-installer <packageName> <packageVersion> [<packageDestination>]" << std::endl;
        return EXIT_FAILURE;
    }

    packageName = argv[1];
    packageVersionRangeStr = argv[2];
    packageDestination = argc >= 4 ? std::optional<std::string>{argv[3]} : std::nullopt;

    const auto packageVersionRange = tinynuget::VersionRange::parse(packageVersionRangeStr);
    if (!packageVersionRange)
    {
        std::cerr << "Invalid version range. Please refer to "
                  << "https://docs.microsoft.com/en-us/nuget/concepts/package-versioning#version-ranges"
                  << " for more information." << std::endl;
        return EXIT_FAILURE;
    }

    try
    {
        tinynuget::Client  client;
        tinynuget::Feed    feed = client.openFeed(tinynuget::default_feed_address).get();
        tinynuget::Package package = feed.resolvePackage(packageName, *packageVersionRange).get();

        if (packageDestination)
        {
            if (!package.installedIn(*packageDestination))
                package.installIn(*packageDestination);
        }
        else
        {
            // install globally only
            if (!package.installed())
                package.install();
        }

        return EXIT_SUCCESS;
    }
    catch (std::exception& exc)
    {
        std::cerr << "Unhandled exception." << std::endl << "Exception: " << exc.what() << std::endl;
        throw;
    }
}

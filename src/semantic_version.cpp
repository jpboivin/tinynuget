//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/semantic_version.h>

#include <algorithm>
#include <charconv>
#include <cstring>
#include <regex>

namespace tinynuget
{
    std::optional<SemanticVersion> SemanticVersion::parse(std::string_view version)
    {
        // from https://semver.org/#is-there-a-suggested-regular-expression-regex-to-check-a-semver-string
        static constexpr std::string_view semver_regex_str =
            R"(^(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$)";
        static const std::regex semver_regex = {semver_regex_str.cbegin(), semver_regex_str.cend()};

        std::cmatch matches;
#if defined(_MSVC_STL_UPDATE)
        if (!version.empty() && std::regex_match(version.data(), version.data() + version.size(), matches, semver_regex))
#else
        if (std::regex_match(version.cbegin(), version.cend(), matches, semver_regex))
#endif
        {
            uint8_t                         major = 0;
            uint8_t                         minor = 0;
            uint8_t                         patch = 0;
            std::optional<std::string_view> label = std::nullopt;
            std::optional<std::string_view> metadata = std::nullopt;

            auto result = std::from_chars_result{};

            result = std::from_chars(matches[1].first, matches[1].second, major);
            if (result.ec != std::errc{})
                return std::nullopt;

            result = std::from_chars(matches[2].first, matches[2].second, minor);
            if (result.ec != std::errc{})
                return std::nullopt;

            result = std::from_chars(matches[3].first, matches[3].second, patch);
            if (result.ec != std::errc{})
                return std::nullopt;

            if (matches[4].matched)
                label = std::string_view{matches[4].first, static_cast<size_t>(matches[4].length())};

            if (matches[5].matched)
                metadata = std::string_view{matches[5].first, static_cast<size_t>(matches[5].length())};

            return SemanticVersion{major, minor, patch, label, metadata};
        }

        return std::nullopt;
    }

    SemanticVersion::SemanticVersion(const uint8_t                          major,
                                     const uint8_t                          minor,
                                     const uint8_t                          patch,
                                     const std::optional<std::string_view>& label,
                                     const std::optional<std::string_view>& metadata)
        : m_major(major)
        , m_minor(minor)
        , m_patch(patch)
        , m_label(label ? std::optional<std::string>{*label} : std::nullopt)
        , m_metadata(metadata ? std::optional<std::string>{*metadata} : std::nullopt)
    {
    }

    SemanticVersion::~SemanticVersion() = default;

    uint8_t SemanticVersion::major() const noexcept
    {
        return m_major;
    }

    uint8_t SemanticVersion::minor() const noexcept
    {
        return m_minor;
    }

    uint8_t SemanticVersion::patch() const noexcept
    {
        return m_patch;
    }

    const std::optional<std::string>& SemanticVersion::label() const noexcept
    {
        return m_label;
    }

    const std::optional<std::string>& SemanticVersion::metadata() const noexcept
    {
        return m_metadata;
    }

    std::string SemanticVersion::normalize() const noexcept
    {
        std::string normalizedVersion;

        normalizedVersion += std::to_string(major());
        normalizedVersion += '.';
        normalizedVersion += std::to_string(minor());
        normalizedVersion += '.';
        normalizedVersion += std::to_string(patch());

        if (label())
        {
            normalizedVersion += '-';
            normalizedVersion += *label();
        }

        return normalizedVersion;
    }

    int SemanticVersion::compare(const SemanticVersion& other) const noexcept
    {
        if (major() != other.major())
            return major() - other.major();

        if (minor() != other.minor())
            return minor() - other.minor();

        if (patch() != other.patch())
            return patch() - other.patch();

        return std::strcmp(label() ? label()->c_str() : "", other.label() ? other.label()->c_str() : "");
    }

    bool operator==(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept
    {
        return lhs.compare(rhs) == 0;
    }

    bool operator!=(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept
    {
        return lhs.compare(rhs) != 0;
    }

    bool operator>(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept
    {
        return lhs.compare(rhs) > 0;
    }

    bool operator>=(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept
    {
        return lhs.compare(rhs) >= 0;
    }

    bool operator<(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept
    {
        return lhs.compare(rhs) < 0;
    }

    bool operator<=(const SemanticVersion& lhs, const SemanticVersion& rhs) noexcept
    {
        return lhs.compare(rhs) <= 0;
    }

} // namespace tinynuget

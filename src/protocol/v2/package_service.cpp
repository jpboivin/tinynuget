//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "protocol/v2/package_service.h"
#include "xml/xml.h"

namespace tinynuget::protocol::v2
{
    PackageService::PackageService(std::string_view serviceUrl) : BasePackageService(serviceUrl)
    {
    }

    std::future<std::vector<SemanticVersion>> PackageService::fetchVersions(std::string_view packageName)
    {
        return std::async(std::launch::async, [this, packageName = std::string{packageName}]() {
            std::vector<SemanticVersion> versions;

            auto response = m_client.fetch<XMLPtr>("/FindPackagesById()?id='" + packageName + "'");
            auto xmlResponse = response.get();

            auto feedElement = xmlResponse->FirstChildElement("feed");
            if (feedElement != nullptr)
            {
                auto entryElement = feedElement->FirstChildElement("entry");
                while (entryElement != nullptr)
                {
                    auto propertiesElement = entryElement->FirstChildElement("m:properties");
                    if (propertiesElement != nullptr)
                    {
                        auto versionElement = propertiesElement->FirstChildElement("d:Version");
                        if (versionElement != nullptr)
                        {
                            auto version = SemanticVersion::parse(versionElement->GetText());
                            if (version)
                                versions.emplace_back(std::move(*version));
                        }
                    }

                    // get the next entry
                    entryElement = entryElement->NextSiblingElement("entry");
                }
            }

            // this means the package does not exist
            // mimick the v3 protocol by throwing a 404
            if (versions.empty())
            {
                HttpStatusException exc;
                exc.statusCode = 404;

                throw exc;
            }

            return versions;
        });
    }

    std::future<Package> PackageService::fetchInfo(std::string_view packageName, const SemanticVersion& packageVersion)
    {
        return std::async(std::launch::async, [this, packageName = std::string{packageName}, packageVersion]() -> Package {
            auto response = m_client.fetch<XMLPtr>("/Packages(Id='" + packageName + "',Version='" + packageVersion.normalize() + "')");
            auto xmlResponse = response.get();

            auto entryElement = xmlResponse->FirstChildElement("entry");
            if (entryElement != nullptr)
            {
                auto contentElement = entryElement->FirstChildElement("content");

                const char* packageUrl = nullptr;
                if (contentElement != nullptr && contentElement->QueryStringAttribute("src", &packageUrl) == tinyxml2::XMLError::XML_SUCCESS)
                {
                    return Package{packageName, packageVersion, packageUrl};
                }

                // TODO load the hash
                // <d:PackageHash>	Edm.String	MY NG NS2 NS3
                // <d:PackageHashAlgorithm>	Edm.String	MY NG NS2 NS3
            }

            throw std::exception{};
        });
    }

} // namespace tinynuget::protocol::v2

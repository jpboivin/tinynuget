//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#ifndef TINYNUGET_PRIVATE_PROTOCOL_V2_PACKAGE_SERVICE_H
#define TINYNUGET_PRIVATE_PROTOCOL_V2_PACKAGE_SERVICE_H

#include "protocol/base_package_service.h"

namespace tinynuget::protocol::v2
{
    /**
     * @brief PackageService is an abstraction of the service(s) manipulating NuGet packages.
     * @sa https://joelverhagen.github.io/NuGetUndocs/#v2-protocol
     */
    class PackageService final : public BasePackageService
    {
    public:
        PackageService(std::string_view serviceUrl);

        //! @copydoc BasePackageService::fetchVersions
        [[nodiscard]] std::future<std::vector<SemanticVersion>> fetchVersions(std::string_view packageName) override;

        //! @copydoc BasePackageService::fetchInfo
        [[nodiscard]] std::future<Package> fetchInfo(std::string_view packageName, const SemanticVersion& packageVersion) override;
    };

} // namespace tinynuget::protocol::v2

#endif // TINYNUGET_PRIVATE_PROTOCOL_V2_PACKAGE_SERVICE_H

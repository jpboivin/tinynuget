//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "http/client.h"
#include "protocol/v2/feed.h"
#include "protocol/v2/package_service.h"
#include "xml/xml.h"

#include <tinynuget/exceptions.h>

using namespace std::string_view_literals;

namespace tinynuget::protocol::v2
{
    std::future<Feed> openFeed(std::string_view address)
    {
        return std::async(std::launch::async, [feedAddress = std::string{address}]() -> Feed {
            http::Client client{feedAddress};

            auto response = client.fetch<XMLPtr>("/");
            auto xmlResponse = response.get();

            auto serviceElement = xmlResponse->FirstChildElement("service");
            if (serviceElement != nullptr)
            {
                auto workspaceElement = serviceElement->FirstChildElement("workspace");
                if (workspaceElement != nullptr)
                {
                    auto collectionElement = workspaceElement->FirstChildElement("collection");
                    if (collectionElement != nullptr)
                    {
                        auto titleElement = collectionElement->FirstChildElement("atom:title");
                        if (titleElement != nullptr && titleElement->GetText() != nullptr && titleElement->GetText() == "Packages"sv)
                        {
                            // looks like a regular NuGet v2 feed
                            return {std::make_unique<PackageService>(feedAddress)};
                        }
                    }
                }
            }

            UnsupportedFeedException exc{std::move(feedAddress)};
            throw exc;
        });
    }

} // namespace tinynuget::protocol::v2

#
# Copyright (C) 2020 Jean-Philippe Boivin. All rights reserved.
#

target_sources(TinyNuget PRIVATE
    feed.h
    feed.cpp
    package_service.h
    package_service.cpp
)
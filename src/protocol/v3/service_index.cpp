//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "json/json.h"
#include "protocol/v3/service_index.h"

namespace tinynuget::protocol::v3
{
    ServiceIndex::ServiceIndex(std::string_view feedAddress) : m_client(feedAddress)
    {
    }

    std::future<std::vector<ServiceIndex::Resource>> ServiceIndex::fetchResources()
    {
        return std::async(std::launch::async, [this]() {
            auto response = m_client.fetch<JSON>("/index.json");
            auto jsonResponse = response.get();
            auto jsonResources = jsonResponse["resources"];

            std::vector<ServiceIndex::Resource> resources;
            for (const auto& jsonResource : jsonResources.array_items())
            {
                Resource resource;
                resource.url = jsonResource["@id"].string_value();
                resource.type = jsonResource["@type"].string_value();
                resource.comment = jsonResource["comment"].string_value();

                resources.emplace_back(std::move(resource));
            }

            return resources;
        });
    }

} // namespace tinynuget::protocol::v3

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "protocol/v3/feed.h"
#include "protocol/v3/package_service.h"
#include "protocol/v3/service_index.h"

#include <tinynuget/exceptions.h>

namespace tinynuget::protocol::v3
{
    std::future<Feed> openFeed(std::string_view address)
    {
        return std::async(std::launch::async, [feedAddress = std::string{address}]() -> Feed {
            auto serviceIndex = protocol::v3::ServiceIndex{feedAddress};
            auto resources = serviceIndex.fetchResources().get();

            std::map<std::string, std::string> indexedResources;
            for (const auto& resource : resources)
            {
                indexedResources.emplace(resource.type, resource.url);
            }

            auto it = indexedResources.find(std::string{protocol::v3::PackageService::type_identifier});
            if (it != indexedResources.cend())
            {
                std::string_view packageServiceUrl = it->second;
                return {std::make_unique<PackageService>(packageServiceUrl)};
            }

            UnsupportedFeedException exc{std::move(feedAddress)};
            throw exc;
        });
    }

} // namespace tinynuget::protocol::v3

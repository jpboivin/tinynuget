//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "json/json.h"
#include "protocol/v3/package_service.h"

namespace tinynuget::protocol::v3
{
    PackageService::PackageService(std::string_view serviceUrl) : BasePackageService(serviceUrl)
    {
    }

    std::future<std::vector<SemanticVersion>> PackageService::fetchVersions(std::string_view packageName)
    {
        return std::async(std::launch::async, [this, packageName = std::string{packageName}]() {
            auto response = m_client.fetch<JSON>(packageName + "/index.json");
            auto jsonResponse = response.get();
            auto jsonVersions = jsonResponse["versions"];

            std::vector<SemanticVersion> versions;
            for (const auto& jsonVersion : jsonVersions.array_items())
            {
                auto version = SemanticVersion::parse(jsonVersion.string_value());
                if (version)
                    versions.emplace_back(std::move(*version));
            }

            return versions;
        });
    }

    std::future<Package> PackageService::fetchInfo(std::string_view packageName, const SemanticVersion& packageVersion)
    {
        std::string packageFilename = std::string{packageName} + '.' + packageVersion.normalize() + ".nupkg";
        std::string packageUrl = m_client.baseAddress() + std::string{packageName} + '/' + packageVersion.normalize() + '/' + packageFilename;

        std::promise<Package> promise;
        promise.set_value({std::string{packageName}, packageVersion, packageUrl});
        return promise.get_future();
    }

} // namespace tinynuget::protocol::v3

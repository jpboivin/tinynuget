//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#ifndef TINYNUGET_PRIVATE_PROTOCOL_V3_FEED_H
#define TINYNUGET_PRIVATE_PROTOCOL_V3_FEED_H

#include <tinynuget/feed.h>

#include <future>
#include <string_view>

namespace tinynuget::protocol::v3
{
    /**
     * @brief Open a NuGet V3 feed.
     * @param[in] address  The address of the feed.
     * @return A future that will hold the opened feed; or an exception on failure.
     * @remarks The feed address must exclude the 'index.json' part.
     */
    [[nodiscard]] std::future<Feed> openFeed(std::string_view address);

} // namespace tinynuget::protocol::v3

#endif // TINYNUGET_PRIVATE_PROTOCOL_V3_FEED_H

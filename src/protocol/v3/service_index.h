//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#ifndef TINYNUGET_PRIVATE_PROTOCOL_V3_SERVICE_INDEX_H
#define TINYNUGET_PRIVATE_PROTOCOL_V3_SERVICE_INDEX_H

#include "http/client.h"

#include <future>
#include <string>
#include <string_view>
#include <vector>

namespace tinynuget::protocol::v3
{
    /**
     \sa https://docs.microsoft.com/en-us/nuget/api/service-index
     */
    class ServiceIndex
    {
    public:
        // v2: https://artifactory.ubisoft.org/api/nuget/nuget
        // v3: https://artifactory.ubisoft.org/api/nuget/v3/nuget

        /**
         \brief A resource represents a versioned capability of a package source.
         */
        struct Resource
        {
            std::string url;     //!< The URL to the resource
            std::string type;    //!< A string constant representing the resource type
            std::string comment; //!< A human readable description of the resource
        };

    public:
        ServiceIndex(std::string_view feedAddress);

        [[nodiscard]] std::future<std::vector<Resource>> fetchResources();

    private:
        http::Client m_client;
    };

} // namespace tinynuget::protocol::v3

#endif // TINYNUGET_PRIVATE_PROTOCOL_V3_SERVICE_INDEX_H

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#ifndef TINYNUGET_PRIVATE_PROTOCOL_BASE_PACKAGE_SERVICE_H
#define TINYNUGET_PRIVATE_PROTOCOL_BASE_PACKAGE_SERVICE_H

#include "http/client.h"

#include <tinynuget/package.h>
#include <tinynuget/semantic_version.h>

#include <future>
#include <string_view>
#include <vector>

namespace tinynuget::protocol
{
    /**
     * @brief PackageService is an abstraction of the service(s) manipulating NuGet packages.
     */
    class BasePackageService
    {
    public:
        BasePackageService(std::string_view serviceUrl);
        virtual ~BasePackageService() = default;

        /**
         * @brief Fetch all the released versions of a NuGet package.
         * @param[in] packageName The name of the package (must be normalized).
         * @return A future that will hold the available versions of the package; or an exception on failure.
         */
        [[nodiscard]] virtual std::future<std::vector<SemanticVersion>> fetchVersions(std::string_view packageName) = 0;

        /**
         * @brief Fetch the information for a specific release of a NuGet package.
         * @param[in] packageName The name of the package (must be normalized).
         * @param[in] packageVersion The version for which to retrieve the informations.
         * @return A future that will hold the package; or an exception on failure.
         */
        [[nodiscard]] virtual std::future<Package> fetchInfo(std::string_view packageName, const SemanticVersion& packageVersion) = 0;

    protected:
        http::Client m_client;
    };

} // namespace tinynuget::protocol

#endif // TINYNUGET_PRIVATE_PROTOCOL_BASE_PACKAGE_SERVICE_H

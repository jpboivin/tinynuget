//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#ifndef TINYNUGET_PRIVATE_UTILITIES_FILESYSTEM_H
#define TINYNUGET_PRIVATE_UTILITIES_FILESYSTEM_H

#include <string>
#include <string_view>

namespace tinynuget::filesystem
{
    std::string normalize(std::string_view path);
    std::string combine(std::string_view firstPath, std::string_view secondPath);
    std::string combine(std::string_view firstPath, std::string_view secondPath, std::string_view thirdPath);

    std::string parent(std::string_view path);

    /**
     * @brief Check whether the path points to an existing file or directory.
     * @param[in] path  The path of the file or directory to check.
     * @return true if the file/directory exists; false otherwise.
     */
    bool exists(std::string_view path);

    /**
     * @brief Ensure that the directory, and all its parents, exist.
     * @param[in] directoryPath  The path to the directory to ensure existance of.
     */
    void ensureDirectoryExists(std::string_view directoryPath);

    bool remove(std::string_view path);
    bool removeAll(std::string_view path);

    namespace nuget
    {
        /**
         * @brief Get the global directory where packages are installed.
         * @return The global directory where packages are installed.
         * @see https://docs.microsoft.com/en-us/nuget/consume-packages/managing-the-global-packages-and-cache-folders
         */
        std::string globalDirectory();

        /**
         * @brief Get the scratch directory where temporary files for NuGet operations can be created.
         * @return The scratch directory for NuGet.
         * @see https://docs.microsoft.com/en-us/nuget/consume-packages/managing-the-global-packages-and-cache-folders
         */
        std::string scratchDirectory();

        /**
         * @brief Get a temporary directory within the scratch directory.
         * @return A new temporary directory.
         * @remarks Every call should generate a new directory name that is likely to be unique.
         */
        std::string tempDirectory();

    } // namespace nuget

} // namespace tinynuget::filesystem

#endif // TINYNUGET_PRIVATE_UTILITIES_FILESYSTEM_H

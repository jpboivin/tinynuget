//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/base64.h"

// clang-format off
#include <windows.h>
#include <wincrypt.h>
// clang-format on

namespace tinynuget::base64
{
    std::string encode(const void* content, const size_t size)
    {
        std::string base64String;

        DWORD encodedSize = 0;
        CryptBinaryToStringA(static_cast<const BYTE*>(content),
                             static_cast<DWORD>(size),
                             CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF,
                             nullptr,
                             &encodedSize);

        if (encodedSize > 0)
        {
            base64String.resize(encodedSize - 1); // the encodedSize includes the NUL character
            CryptBinaryToStringA(static_cast<const BYTE*>(content),
                                 static_cast<DWORD>(size),
                                 CRYPT_STRING_BASE64 | CRYPT_STRING_NOCRLF,
                                 base64String.data(),
                                 &encodedSize);
        }

        return base64String;
    }

} // namespace tinynuget::base64

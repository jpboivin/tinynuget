//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/filesystem.h"

#include <cassert>

#include <ftw.h>
#include <pwd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

namespace tinynuget::filesystem
{
    constexpr char separator = '/';

    std::string parent(std::string_view path)
    {
        std::string parentFolder = normalize(path);
        if (!parentFolder.empty() && parentFolder.back() == separator)
            parentFolder.pop_back();
        return parentFolder.substr(0, parentFolder.rfind(separator));
    }

    bool exists(std::string_view path)
    {
        struct stat fileInfo;
        return stat(std::string{path}.c_str(), &fileInfo) == 0;
    }

    void ensureDirectoryExists(std::string_view directoryPath)
    {
        std::string normalizedPath = normalize(directoryPath);

        // create intermediate directories
        if (!normalizedPath.empty())
        {
            size_t separatorPos = 0;
            while ((separatorPos = normalizedPath.find(separator, separatorPos + 1)) != std::string::npos)
            {
                std::string partialPath = normalizedPath.substr(0, separatorPos);

                if (!filesystem::exists(partialPath))
                {
                    [[maybe_unused]] int ret = mkdir(partialPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                    assert(ret == 0);
                }
            }
        }

        // create the final directory
        if (!filesystem::exists(normalizedPath))
        {
            [[maybe_unused]] int ret = mkdir(normalizedPath.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
            assert(ret == 0);
        }
    }

    bool remove(std::string_view path)
    {
        struct stat statInfo;
        if (stat(std::string{path}.c_str(), &statInfo) == 0)
        {
            if (S_ISDIR(statInfo.st_mode))
                return rmdir(std::string{path}.c_str()) == 0;
            else
                return unlink(std::string{path}.c_str()) == 0;
        }

        return false;
    }

    bool removeAll(std::string_view path)
    {
        struct stat statInfo;
        if (stat(std::string{path}.c_str(), &statInfo) == 0)
        {
            if (S_ISDIR(statInfo.st_mode))
            {
                auto removeCallback = [](const char* name, const struct stat*, int, struct FTW*) -> int { return remove(name) ? 0 : 1; };
                return nftw(std::string{path}.c_str(), removeCallback, 64, FTW_DEPTH | FTW_PHYS) == 0;
            }
            else
            {
                return unlink(std::string{path}.c_str()) == 0;
            }
        }

        return false;
    }

    namespace nuget
    {
        std::string globalDirectory()
        {
            const char* globalDirectory = getenv("NUGET_PACKAGES");

            if (globalDirectory == nullptr)
            {
                const char* homeDirectory = getenv("HOME");
                if (homeDirectory == nullptr)
                    homeDirectory = getpwuid(getuid())->pw_dir;
                return filesystem::combine(homeDirectory, ".nuget", "packages");
            }

            return globalDirectory;
        }

        std::string scratchDirectory()
        {
            return "/tmp/NuGetScratch";
        }

    } // namespace nuget

} // namespace tinynuget::filesystem

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/filesystem.h"
#include "utilities/sha512.h"

#include <algorithm>
#include <atomic>
#include <cstdio>
#include <ctime>
#include <sstream>
#include <thread>

namespace tinynuget::filesystem
{
    constexpr char separator = '/';

    std::string normalize(std::string_view path)
    {
        std::string normalizedPath{path};
        std::transform(normalizedPath.cbegin(), normalizedPath.cend(), normalizedPath.begin(), [](const char c) {
            return c == '\\' ? separator : c;
        });
        return normalizedPath;
    }

    std::string combine(std::string_view firstPath, std::string_view secondPath)
    {
        std::string normalizedFirstPath = normalize(firstPath);
        std::string normalizedSecondPath = normalize(secondPath);

        // TODO more intelligent code...
        std::string combinedPath;
        combinedPath += (normalizedFirstPath.empty() || normalizedFirstPath.back() != separator)
                            ? normalizedFirstPath
                            : normalizedFirstPath.substr(0, normalizedFirstPath.size() - 1);
        if (!normalizedFirstPath.empty())
            combinedPath += separator;
        combinedPath += (normalizedSecondPath.empty() || normalizedSecondPath.front() != separator) ? normalizedSecondPath
                                                                                                    : normalizedSecondPath.substr(1);

        return combinedPath;
    }

    std::string combine(std::string_view firstPath, std::string_view secondPath, std::string_view thirdPath)
    {
        return combine(combine(firstPath, secondPath), thirdPath);
    }

    namespace nuget
    {
        static std::atomic<int> g_tempDirectoryValue = {0};

        std::string tempDirectory()
        {
            std::string tempName;

            {
                uint8_t digest[hash::SHA512::digest_size];

                std::stringstream ss;
                ss << (++g_tempDirectoryValue) << '_' << std::time(nullptr) << '_' << std::this_thread::get_id();

                std::string str = ss.str();

                hash::SHA512 hash;
                hash.update(str.data(), str.size());
                hash.finalize(digest);

                char hexDigest[65];
                for (size_t i = 0; i < sizeof(hexDigest) / 2; ++i)
                    std::sprintf(&hexDigest[i * 2], "%02x", digest[i]);

                tempName = {hexDigest, sizeof(hexDigest) - 1};
            }

            return filesystem::combine(scratchDirectory(), tempName);
        }

    } // namespace nuget

} // namespace tinynuget::filesystem

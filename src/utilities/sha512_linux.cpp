//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#if __has_include(<linux/if_alg.h>)

#    include "utilities/sha512.h"

#    include <cstring>
#    include <new>

#    include <sys/socket.h>
#    include <unistd.h>

#    include <linux/if_alg.h>
#    include <linux/socket.h>

namespace tinynuget::hash
{
    namespace
    {
        struct SHA512Context
        {
            int fd;
        };

    } // namespace

    SHA512::SHA512()
    {
        static constexpr char algorithm_type[] = "hash";
        static constexpr char algorithm_name[] = "sha512";

        // ensure an object exist
        ::new (&m_ctx) SHA512Context();

        struct sockaddr_alg sa = {};
        sa.salg_family = AF_ALG;
        std::memcpy(sa.salg_type, algorithm_type, sizeof(algorithm_type));
        std::memcpy(sa.salg_name, algorithm_name, sizeof(algorithm_name));

        int sockfd = socket(AF_ALG, SOCK_SEQPACKET, 0);
        bind(sockfd, reinterpret_cast<struct sockaddr*>(&sa), sizeof(sa));
        {
            auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);
            ctx.fd = accept(sockfd, nullptr, 0);
        }
        close(sockfd);
    }

    SHA512::~SHA512()
    {
        auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);
        close(ctx.fd);

        ctx.~SHA512Context();
    }

    void SHA512::update(const void* data, const size_t size)
    {
        auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);
        send(ctx.fd, data, size, MSG_MORE);
    }

    void SHA512::finalize(unsigned char* outDigest)
    {
        auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);
        write(ctx.fd, nullptr, 0); // finalize the digest
        read(ctx.fd, outDigest, digest_size);
    }

} // namespace tinynuget::hash

#else

#    include "utilities/sha512.h"

#    include <cstdint>
#    include <cstring>
#    include <new>

#    include <endian.h>

#    define LITTLE_ENDIAN __LITTLE_ENDIAN
#    define BIG_ENDIAN    __BIG_ENDIAN
#    define BYTE_ORDER    __BYTE_ORDER

#    include "utilities/sha512_generic.c"

namespace tinynuget::hash
{
    SHA512::SHA512()
    {
        // ensure an object exist
        ::new (&m_ctx) SHA512_CTX;

        auto& ctx = *reinterpret_cast<SHA512_CTX*>(&m_ctx);
        SHA512_Init(&ctx);
    }

    SHA512::~SHA512()
    {
        auto& ctx = *reinterpret_cast<SHA512_CTX*>(&m_ctx);
        ctx.~SHA512_CTX();
    }

    void SHA512::update(const void* data, const size_t size)
    {
        auto& ctx = *reinterpret_cast<SHA512_CTX*>(&m_ctx);
        SHA512_Update(&ctx, const_cast<void*>(data), size);
    }

    void SHA512::finalize(unsigned char* outDigest)
    {
        auto& ctx = *reinterpret_cast<SHA512_CTX*>(&m_ctx);
        SHA512_Final(outDigest, &ctx);
    }

} // namespace tinynuget::hash

#endif

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#ifndef TINYNUGET_PRIVATE_UTILITIES_SHA512_H
#define TINYNUGET_PRIVATE_UTILITIES_SHA512_H

#include <cstddef>
#include <type_traits>

namespace tinynuget::hash
{
    /**
     * @brief SHA-512 hash algorithm.
     */
    class SHA512 final
    {
    public:
        /**
         * @brief The size (in bytes) of the SHA-512 digest.
         */
        static constexpr size_t digest_size = 64;

        SHA512();
        ~SHA512();

        SHA512(const SHA512& other) = delete;
        SHA512& operator=(const SHA512& other) = delete;

        void update(const void* data, size_t size);
        void finalize(unsigned char* outDigest);

    private:
        static constexpr size_t                                 context_size = 256;
        static constexpr size_t                                 context_alignment = alignof(std::max_align_t);
        std::aligned_storage_t<context_size, context_alignment> m_ctx;
    };

} // namespace tinynuget::hash

#endif // TINYNUGET_PRIVATE_UTILITIES_SHA512_H

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/zip.h"

#include <algorithm>
#include <cstring>

// clang-format off
#include <windows.h>
#include <shellapi.h>
#include <shldisp.h>
// clang-format on

namespace tinynuget::zip
{
    namespace
    {
        class COMString
        {
        public:
            COMString(std::string_view str)
            {
                m_size = MultiByteToWideChar(CP_ACP, 0, str.data(), static_cast<int>(str.size()), NULL, 0);
                m_str = SysAllocStringLen(NULL, m_size);
                MultiByteToWideChar(CP_ACP, 0, str.data(), static_cast<int>(str.size()), m_str, m_size);
            }

            COMString(std::wstring_view str)
            {
                static_assert(sizeof(std::wstring_view::value_type) == sizeof(OLECHAR));
                m_size = static_cast<int>(str.size());
                m_str = SysAllocStringLen(NULL, m_size);
                std::memcpy(m_str, str.data(), str.size() * sizeof(std::wstring_view::value_type));
            }

            ~COMString()
            {
                SysFreeString(m_str);
            }

            size_t size() const noexcept
            {
                return static_cast<size_t>(m_size);
            }

            const wchar_t* c_str() const noexcept
            {
                return m_str;
            }

            const BSTR bstr() const noexcept
            {
                return m_str;
            }

        private:
            BSTR m_str;
            int  m_size;
        };

    } // namespace

    bool extract(const COMString& zipFile, const COMString& destination)
    {
        bool success = false;

        // ensure the file ends with .zip, the Windows shell is extension based
        COMString safeZipFile{std::wstring{zipFile.c_str(), zipFile.size()} + L".zip"};
        MoveFileW(zipFile.c_str(), safeZipFile.c_str());

        CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
        {
            IShellDispatch* shell = nullptr;
            Folder*         zippedDirectory = nullptr;
            FolderItems*    zippedFiles = nullptr;
            IDispatch*      zippedFilesDispatch = nullptr;
            Folder*         destinationDirectory = nullptr;

            // 1. Create the shell instance
            if (SUCCEEDED(CoCreateInstance(CLSID_Shell, NULL, CLSCTX_INPROC_SERVER, IID_IShellDispatch, reinterpret_cast<LPVOID*>(&shell))))
            {
                // 2. Open the zip file in the shell (as a zipped directory)
                VARIANT vZipFile;
                vZipFile.vt = VT_BSTR;
                vZipFile.bstrVal = const_cast<BSTR>(safeZipFile.bstr());

                if (SUCCEEDED(shell->NameSpace(vZipFile, &zippedDirectory)))
                {
                    // 3. Open the destination the the shell
                    VARIANT vDestination;
                    vDestination.vt = VT_BSTR;
                    vDestination.bstrVal = const_cast<BSTR>(destination.bstr());

                    if (SUCCEEDED(shell->NameSpace(vDestination, &destinationDirectory)))
                    {
                        // 4. Get the zipped files
                        if (SUCCEEDED(zippedDirectory->Items(&zippedFiles)))
                        {
                            // 5. Get the zipped files dispatcher
                            zippedFiles->QueryInterface(IID_IDispatch, reinterpret_cast<LPVOID*>(&zippedFilesDispatch));

                            // 6. Copy the zipped files to the destination
                            VARIANT vZippedItems;
                            vZippedItems.vt = VT_DISPATCH;
                            vZippedItems.pdispVal = zippedFilesDispatch;

                            VARIANT vOptions;
                            vOptions.vt = VT_I4;
                            vOptions.lVal = 1024 | 512 | 16 | 4; // http://msdn.microsoft.com/en-us/library/bb787866(VS.85).aspx
                            success = SUCCEEDED(destinationDirectory->CopyHere(vZippedItems, vOptions));
                        }
                    }
                }
            }

            if (destinationDirectory != nullptr)
                destinationDirectory->Release();
            if (zippedFilesDispatch != nullptr)
                zippedFilesDispatch->Release();
            if (zippedFiles != nullptr)
                zippedFiles->Release();
            if (zippedDirectory != nullptr)
                zippedDirectory->Release();
            if (shell != nullptr)
                shell->Release();
        }
        CoUninitialize();

        MoveFileW(safeZipFile.c_str(), zipFile.c_str()); // back to original name
        return success;
    }

    bool extract(std::string_view zipFile, std::string_view destination)
    {
        // Within this library, we normalize all paths to forward slashes.
        // The Windows Shell API is one of the rare API that only works with backslashes.
        // We hide this requirement here.
        std::string normalizedZipFile{zipFile};
        std::string normalizedDestination{destination};

        std::transform(normalizedZipFile.cbegin(), normalizedZipFile.cend(), normalizedZipFile.begin(), [](const char c) {
            return c == '/' ? '\\' : c;
        });
        std::transform(normalizedDestination.cbegin(), normalizedDestination.cend(), normalizedDestination.begin(), [](const char c) {
            return c == '/' ? '\\' : c;
        });

        return extract(COMString{normalizedZipFile}, COMString{normalizedDestination});
    }

} // namespace tinynuget::zip

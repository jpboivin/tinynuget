//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/sha512.h"

#include <new>

#include <CommonCrypto/CommonCrypto.h>
#include <CommonCrypto/CommonDigest.h>

namespace tinynuget::hash
{
    SHA512::SHA512()
    {
        // ensure an object exist
        ::new (&m_ctx) CC_SHA512_CTX;

        auto& ctx = *reinterpret_cast<CC_SHA512_CTX*>(&m_ctx);
        CC_SHA512_Init(&ctx);
    }

    SHA512::~SHA512()
    {
        auto& ctx = *reinterpret_cast<CC_SHA512_CTX*>(&m_ctx);
        ctx.~CC_SHA512_CTX();
    }

    void SHA512::update(const void* data, const size_t size)
    {
        auto& ctx = *reinterpret_cast<CC_SHA512_CTX*>(&m_ctx);
        CC_SHA512_Update(&ctx, data, static_cast<CC_LONG>(size));
    }

    void SHA512::finalize(unsigned char* outDigest)
    {
        auto& ctx = *reinterpret_cast<CC_SHA512_CTX*>(&m_ctx);
        CC_SHA512_Final(outDigest, &ctx);
    }

} // namespace tinynuget::hash

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#if defined(TINYNUGET_HAS_MINIZIP)

#    include "utilities/filesystem.h"
#    include "utilities/zip.h"

#    include <cstring>
#    include <string>

#    include <sys/time.h>
#    include <unistd.h>
#    include <utime.h>

#    include <mz.h>
#    include <mz_os.h>
#    include <mz_strm_os.h>
#    include <mz_zip.h>

namespace tinynuget::zip
{
    bool extract(std::string_view zipFile, std::string_view destination)
    {
        bool success = true;

        void* streamHandle = mz_stream_os_create(nullptr);
        if (mz_stream_os_open(streamHandle, std::string{zipFile}.c_str(), MZ_OPEN_MODE_READ) == MZ_OK)
        {
            void* zipHandle = mz_zip_create(&zipHandle);
            if (mz_zip_open(zipHandle, streamHandle, MZ_OPEN_MODE_READ) == MZ_OK)
            {
                int32_t entryIdx = mz_zip_goto_first_entry(zipHandle);
                while (success && entryIdx != MZ_END_OF_LIST)
                {
                    mz_zip_file* info = nullptr;
                    if (mz_zip_entry_get_info(zipHandle, &info) == MZ_OK)
                    {
                        std::string destinationFilename = filesystem::combine(destination, info->filename);
                        FILE*       destinationFile = nullptr;

                        // Note. Some ZIP archives won't have any folder entries
                        //       Always ensure the parent folder exists.
                        std::string destinationFolder = filesystem::parent(destinationFilename);
                        filesystem::ensureDirectoryExists(destinationFolder);

                        if (mz_zip_entry_is_dir(zipHandle) == MZ_OK)
                        {
                            // create the directory
                            filesystem::ensureDirectoryExists(destinationFilename);
                        }
                        else if (mz_zip_entry_is_symlink(zipHandle) == MZ_OK ||
                                 mz_zip_attrib_is_symlink(info->external_fa, info->version_madeby) == MZ_OK)
                        {
                            char linkname[PATH_MAX] = {};
                            std::strncpy(linkname, info->linkname, sizeof(linkname));

                            if (linkname[0] == '\0')
                            {
                                // some zippers (like macOS's Archive Utility) doesn't set the linkname in the ZIP file,
                                // instead it is part of the entry data
                                mz_zip_entry_read_open(zipHandle, 0, nullptr);
                                {
                                    int32_t read = mz_zip_entry_read(zipHandle, linkname, sizeof(linkname) - 1);
                                    if (read > 0)
                                        linkname[read] = '\0';
                                }
                                mz_zip_entry_read_close(zipHandle, nullptr, nullptr, nullptr);
                            }

                            success = symlink(linkname, destinationFilename.c_str()) == 0;
                        }
                        else
                        {
                            if (mz_zip_entry_read_open(zipHandle, 0, nullptr) == MZ_OK)
                            {
                                uint8_t buffer[4096];
                                int32_t read;

                                destinationFile = std::fopen(destinationFilename.c_str(), "wb");
                                if (destinationFile != nullptr)
                                {
                                    while ((read = mz_zip_entry_read(zipHandle, buffer, sizeof(buffer))) > 0)
                                        success &= std::fwrite(buffer, sizeof(uint8_t), read, destinationFile) == static_cast<size_t>(read);
                                    std::fclose(destinationFile);

                                    struct timeval times[2];
                                    times[0].tv_sec = info->accessed_date;
                                    times[0].tv_usec = 0; // not stored in ZIP file
                                    times[1].tv_sec = info->modified_date;
                                    times[1].tv_usec = 0; // not stored in ZIP file

                                    utimes(destinationFilename.c_str(), times);
                                }
                                else
                                {
                                    success = false;
                                }

                                mz_zip_entry_read_close(zipHandle, nullptr, nullptr, nullptr);
                            }
                            else
                            {
                                success = false;
                            }
                        }

                        uint32_t attributes;
                        mz_zip_attrib_convert(MZ_HOST_SYSTEM(info->version_madeby), info->external_fa, MZ_HOST_SYSTEM_UNIX, &attributes);
                        mz_os_set_file_attribs(destinationFilename.c_str(), attributes);
                    }
                    else
                    {
                        success = false;
                    }

                    entryIdx = mz_zip_goto_next_entry(zipHandle);
                }

                mz_zip_close(zipHandle);
            }
            mz_zip_delete(&zipHandle);

            mz_stream_os_close(streamHandle);
        }
        mz_stream_os_delete(&streamHandle);

        return success;
    }

} // namespace tinynuget::zip

#endif

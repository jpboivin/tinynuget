//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/filesystem.h"

#include <cassert>

// clang-format off
#include <windows.h>
#include <shobjidl_core.h>
// clang-format on

namespace tinynuget::filesystem
{
    constexpr char separator = '/';

    std::string parent(std::string_view path)
    {
        std::string parentFolder = normalize(path);
        if (!parentFolder.empty() && parentFolder.back() == separator)
            parentFolder.pop_back();
        return parentFolder.substr(0, parentFolder.rfind(separator));
    }

    bool exists(std::string_view path)
    {
        return GetFileAttributesA(std::string{path}.c_str()) != INVALID_FILE_ATTRIBUTES;
    }

    void ensureDirectoryExists(std::string_view directoryPath)
    {
        std::string normalizedPath = normalize(directoryPath);

        // create intermediate directories
        if (!normalizedPath.empty())
        {
            size_t separatorPos = 0;
            while ((separatorPos = normalizedPath.find(separator, separatorPos + 1)) != std::string::npos)
            {
                std::string partialPath = normalizedPath.substr(0, separatorPos);

                // skip the drive letter
                if (!partialPath.empty() && partialPath.back() == ':')
                    continue;

                if (!filesystem::exists(partialPath))
                {
                    // TODO support Unicode
                    [[maybe_unused]] BOOL success = CreateDirectoryA(partialPath.c_str(), NULL);
                    assert(success);
                }
            }
        }

        // create the final directory
        if (!filesystem::exists(normalizedPath))
        {
            // TODO support Unicode
            [[maybe_unused]] BOOL success = CreateDirectoryA(normalizedPath.c_str(), NULL);
            assert(success);
        }
    }

    bool remove(std::string_view path)
    {
        const DWORD attributes = GetFileAttributesA(std::string{path}.c_str());
        if (attributes != INVALID_FILE_ATTRIBUTES)
        {
            if (attributes & FILE_ATTRIBUTE_DIRECTORY)
                return RemoveDirectoryA(std::string{path}.c_str());
            else
                return DeleteFileA(std::string{path}.c_str());
        }

        return false;
    }

    bool removeAll(std::string_view path)
    {
        bool success = false;

        std::wstring widePath;
        widePath.resize(MultiByteToWideChar(CP_ACP, 0, path.data(), static_cast<int>(path.size()), NULL, 0));
        MultiByteToWideChar(CP_ACP, 0, path.data(), static_cast<int>(path.size()), widePath.data(), static_cast<int>(widePath.size()));

        WCHAR absolutePath[4096] = {};
        GetFullPathNameW(widePath.c_str(), sizeof(absolutePath) / sizeof(absolutePath[0]), absolutePath, NULL);

        CoInitializeEx(NULL, COINIT_APARTMENTTHREADED);
        {
            IFileOperation* fileOp = nullptr;
            IShellItem*     directory = nullptr;

            if (SUCCEEDED(CoCreateInstance(CLSID_FileOperation, NULL, CLSCTX_ALL, IID_PPV_ARGS(&fileOp))))
            {
                if (SUCCEEDED(SHCreateItemFromParsingName(absolutePath, NULL, IID_PPV_ARGS(&directory))))
                {
                    fileOp->DeleteItem(directory, NULL);

                    fileOp->SetOperationFlags(FOF_NO_UI);
                    success = SUCCEEDED(fileOp->PerformOperations());
                }

                if (directory != nullptr)
                    directory->Release();
                if (fileOp != nullptr)
                    fileOp->Release();
            }
        }
        CoUninitialize();

        return success;
    }

    namespace nuget
    {
        std::string globalDirectory()
        {
            char globalDirectory[MAX_PATH] = {};
            if (GetEnvironmentVariableA("NUGET_PACKAGES", globalDirectory, sizeof(globalDirectory)) == 0)
            {
                char userDirectory[MAX_PATH] = {};
                GetEnvironmentVariableA("USERPROFILE", userDirectory, sizeof(userDirectory));
                return filesystem::combine(userDirectory, ".nuget", "packages");
            }

            return globalDirectory;
        }

        std::string scratchDirectory()
        {
            char tempDirectory[MAX_PATH] = {};
            GetEnvironmentVariableA("TEMP", tempDirectory, sizeof(tempDirectory));
            return filesystem::combine(tempDirectory, "NuGetScratch");
        }

    } // namespace nuget

} // namespace tinynuget::filesystem

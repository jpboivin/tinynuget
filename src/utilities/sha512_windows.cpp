//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "utilities/sha512.h"

#include <new>

// clang-format off
#include <windows.h>
#include <bcrypt.h>
// clang-format on

namespace tinynuget::hash
{
    namespace
    {
        struct SHA512Context
        {
            BCRYPT_ALG_HANDLE  providerHandle = NULL;
            BCRYPT_HASH_HANDLE hashHandle = NULL;
        };

    } // namespace

    SHA512::SHA512()
    {
        // ensure an object exist
        ::new (&m_ctx) SHA512Context();

        auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);
        BCryptOpenAlgorithmProvider(&ctx.providerHandle, BCRYPT_SHA512_ALGORITHM, NULL, 0);
        BCryptCreateHash(ctx.providerHandle, &ctx.hashHandle, NULL, 0, NULL, 0, 0);
    }

    SHA512::~SHA512()
    {
        auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);

        if (ctx.hashHandle != NULL)
            BCryptDestroyHash(ctx.hashHandle);
        if (ctx.providerHandle != NULL)
            BCryptCloseAlgorithmProvider(ctx.providerHandle, 0);

        ctx.~SHA512Context();
    }

    void SHA512::update(const void* data, const size_t size)
    {
        auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);
        BCryptHashData(ctx.hashHandle, static_cast<PUCHAR>(const_cast<void*>(data)), static_cast<ULONG>(size), 0);
    }

    void SHA512::finalize(unsigned char* outDigest)
    {
        auto& ctx = *reinterpret_cast<SHA512Context*>(&m_ctx);

        ULONG digestSize = digest_size;
        BCryptFinishHash(ctx.hashHandle, reinterpret_cast<PUCHAR>(outDigest), digestSize, 0);
    }

} // namespace tinynuget::hash

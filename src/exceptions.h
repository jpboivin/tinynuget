//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#ifndef TINYNUGET_PRIVATE_EXCEPTIONS_H
#define TINYNUGET_PRIVATE_EXCEPTIONS_H

#include <exception>
#include <string>

namespace tinynuget
{
    struct SystemException : std::exception
    {
        [[nodiscard]] const char* what() const noexcept override = 0;
    };

    struct NSErrorException final : public SystemException
    {
        int64_t     code;        //!< The code of the NSError
        std::string domain;      //!< The domain of the NSError
        std::string description; //!< The human-readable description of the NSError

        [[nodiscard]] const char* what() const noexcept override;
    };

    struct CurlException final : public SystemException
    {
        int         code;        //!< The code of the CURL error (aka CURLcode).
        std::string description; //!< The human-readable description of the CURLcode.

        [[nodiscard]] const char* what() const noexcept override;
    };

    struct WinHttpException final : public SystemException
    {
        unsigned long code;        //!< The code of the WinHTTP error.
        std::string   description; //!< The human-readable description of the WinHTTP error.

        [[nodiscard]] const char* what() const noexcept override;
    };

    struct HttpStatusException final : std::exception
    {
        std::string resourceUrl; //!< The URL of the HTTP resource.
        uint16_t    statusCode;  //!< The non-success HTTP status code.

        [[nodiscard]] const char* what() const noexcept override;
    };

    struct JsonFormatException final : std::exception
    {
        std::string description; //!< The description of the format errors.

        [[nodiscard]] const char* what() const noexcept override;
    };

    struct XmlFormatException final : std::exception
    {
        int         code;        //!< The code of the TinyXML2 error (aka tinyxml2::XMLError).
        std::string description; //!< The description of the format errors.

        [[nodiscard]] const char* what() const noexcept override;
    };

} // namespace tinynuget

#endif // TINYNUGET_PRIVATE_EXCEPTIONS_H

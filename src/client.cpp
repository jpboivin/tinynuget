//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#if defined(TINYNUGET_HAS_V2_PROTOCOL)
#    include "protocol/v2/feed.h"
#endif

#if defined(TINYNUGET_HAS_V3_PROTOCOL)
#    include "protocol/v3/feed.h"
#endif

#include <tinynuget/client.h>
#include <tinynuget/exceptions.h>

namespace tinynuget
{
    std::future<Feed> Client::openFeed(std::string_view address, const ProtocolVersion protocolVersion)
    {
        switch (protocolVersion)
        {
            case ProtocolVersion::any:
            {
                return std::async(std::launch::async, [address = std::string{address}]() -> Feed {

#if defined(TINYNUGET_HAS_V3_PROTOCOL)
                    try
                    {
                        return protocol::v3::openFeed(address).get();
                    }
                    catch (...)
                    {
                        // nothing, fallback to the next version
                    }
#endif

#if defined(TINYNUGET_HAS_V2_PROTOCOL)
                    try
                    {
                        return protocol::v2::openFeed(address).get();
                    }
                    catch (...)
                    {
                        // nothing, fallback to the next version
                    }
#endif

                    UnsupportedFeedException exc{std::move(address)};
                    throw exc;
                });
            }

#if defined(TINYNUGET_HAS_V2_PROTOCOL)
            case ProtocolVersion::v2:
                return protocol::v2::openFeed(address);
#endif
#if defined(TINYNUGET_HAS_V3_PROTOCOL)
            case ProtocolVersion::v3:
                return protocol::v3::openFeed(address);
#endif

            default:
                std::promise<Feed> promise;
                promise.set_exception(std::make_exception_ptr(UnsupportedProtocolException{protocolVersion}));
                return promise.get_future();
        }
    }

} // namespace tinynuget

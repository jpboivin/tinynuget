//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include <tinynuget/version_range.h>

#include <algorithm>
#include <charconv>

namespace tinynuget
{
    namespace
    {
        // version ranges must "soft" parse the versions
        std::optional<SemanticVersion> parseVersion(std::string_view version)
        {
            const char* last = version.data() + version.size();

            auto result = std::from_chars_result{};
            result.ptr = version.data();

            uint8_t major = 0;
            uint8_t minor = 0;
            uint8_t patch = 0;

            // parse the major version
            result = std::from_chars(result.ptr, last, major);
            if (result.ec != std::errc{})
                return std::nullopt;

            if (result.ptr == last)
                return SemanticVersion{major};

            // parse the minor version
            if (*result.ptr != '.' || ++result.ptr == last)
                return std::nullopt;

            result = std::from_chars(result.ptr, last, minor);
            if (result.ec != std::errc{})
                return std::nullopt;

            if (result.ptr == last)
                return SemanticVersion{major, minor};

            // parse the patch version
            if (*result.ptr != '.' || ++result.ptr == last)
                return std::nullopt;

            result = std::from_chars(result.ptr, last, patch);
            if (result.ec != std::errc{})
                return std::nullopt;

            if (result.ptr == last)
                return SemanticVersion{major, minor, patch};

            // TODO pre-release label

            return std::nullopt;
        }

    } // namespace

    std::optional<VersionRange> VersionRange::parse(std::string_view range)
    {
        // 1. 1.0          x ≥ 1.0         Minimum version, inclusive
        // 2. (1.0,)       x > 1.0         Minimum version, exclusive
        // 3. [1.0]        x == 1.0        Exact version match
        // 4. (,1.0]       x ≤ 1.0         Maximum version, inclusive
        // 5. (,1.0)       x < 1.0         Maximum version, exclusive
        // 6. [1.0,2.0]    1.0 ≤ x ≤ 2.0   Exact range, inclusive
        // 7. (1.0,2.0)    1.0 < x < 2.0   Exact range, exclusive
        // 8. [1.0,2.0)    1.0 ≤ x < 2.0   Mixed inclusive minimum and exclusive maximum version

        // When using the PackageReference format, NuGet also supports using a floating notation, *, for Major, Minor, Patch, and
        // pre-release suffix parts of the number.

        std::optional<SemanticVersion> minimumVersion = std::nullopt;
        VersionComparator              minimumCompare = nullptr;
        std::optional<SemanticVersion> maximumVersion = std::nullopt;
        VersionComparator              maximumCompare = nullptr;
        bool                           valid = false;

        if (!range.empty())
        {
            // first, try to handle the floating notation
            const auto wildcardPos = range.find('*');
            if (wildcardPos != std::string_view::npos)
            {
                // wildcard must be the last character
                if (&range.front() + wildcardPos == &range.back())
                {
                    std::string floatingVersion{range};
                    floatingVersion[wildcardPos] = '0';

                    minimumVersion = parseVersion(floatingVersion);
                    minimumCompare = tinynuget::operator<=;

                    // not any version, must have a maximum
                    if (minimumVersion && minimumVersion->major() > 0)
                    {
                        const auto dotCount = std::count_if(range.cbegin(), range.cend(), [](char c) { return c == '.'; });

                        // if there's a single dot, it means we have x.* and the maximum is (x+1).0.0
                        // if there's two dots, it means we have x.y.* and the maximum is x.(y+1).0
                        const auto maximumMajorVersion =
                            static_cast<uint8_t>(dotCount == 1 ? minimumVersion->major() + 1 : minimumVersion->major());
                        const auto maximumMinorVersion = static_cast<uint8_t>(dotCount == 2 ? minimumVersion->minor() + 1 : 0);

                        maximumVersion = SemanticVersion{maximumMajorVersion, maximumMinorVersion, 0};
                        maximumCompare = tinynuget::operator<;
                    }

                    valid = minimumVersion.has_value();
                }
            }
            else
            {
                // no range delimiters? try the case 1: inclusive minimum version
                if (range.front() != '(' && range.front() != '[')
                {
                    minimumVersion = parseVersion(range);
                    minimumCompare = tinynuget::operator<=;

                    valid = minimumVersion.has_value();
                }
                else
                {
                    minimumCompare = range.front() == '(' ? tinynuget::operator< : tinynuget::operator<=;
                    maximumCompare = range.back() == ')' ? tinynuget::operator< : tinynuget::operator<=;

                    // no comma? try the case 3: exact version match
                    if (range.find(',') == std::string::npos)
                    {
                        if (range.front() == '[' && range.back() == ']')
                        {
                            auto version = parseVersion(range.substr(1, range.size() - 2));
                            minimumVersion = version;
                            maximumVersion = version;

                            valid = minimumVersion.has_value() && maximumVersion.has_value();
                        }
                    }
                    else
                    {
                        range = range.substr(1, range.size() - 2); // strip the range delimiters
                        const auto commaPos = range.find(',');

                        auto minimum = range.substr(0, commaPos);
                        auto maximum = range.substr(commaPos + 1);
                        minimumVersion = parseVersion(minimum);
                        maximumVersion = parseVersion(maximum);

                        valid = (!minimum.empty() == minimumVersion.has_value()) && (!maximum.empty() == maximumVersion.has_value()) &&
                                (!minimum.empty() || !maximum.empty());
                    }
                }
            }
        }

        return valid ? std::optional<VersionRange>{VersionRange{std::move(minimumVersion),
                                                                minimumCompare,
                                                                std::move(maximumVersion),
                                                                maximumCompare}}
                     : std::nullopt;
    }

    VersionRange::VersionRange(std::optional<SemanticVersion> minimumVersion,
                               VersionComparator              minimumCompare,
                               std::optional<SemanticVersion> maximumVersion,
                               VersionComparator              maximumCompare)
        : m_minimumVersion(std::move(minimumVersion))
        , m_minimumCompare(minimumCompare)
        , m_maximumVersion(std::move(maximumVersion))
        , m_maximumCompare(maximumCompare)
    {
    }

    bool VersionRange::in(const SemanticVersion& version) const noexcept
    {
        bool inRange = true;

        if (m_minimumVersion)
            inRange &= m_minimumCompare(*m_minimumVersion, version);
        if (m_maximumVersion)
            inRange &= m_maximumCompare(version, *m_maximumVersion);

        return inRange;
    }

    std::string VersionRange::toString() const noexcept
    {
        std::string rangeStr;

        if (m_minimumVersion)
        {
            rangeStr += m_minimumVersion->normalize();
            rangeStr += m_minimumCompare == tinynuget::operator<= ? " <= " : " < ";
        }

        rangeStr += "version";

        if (m_maximumVersion)
        {
            rangeStr += m_maximumCompare == tinynuget::operator<= ? " <= " : " < ";
            rangeStr += m_maximumVersion->normalize();
        }

        return rangeStr;
    }

} // namespace tinynuget

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "protocol/base_package_service.h"

#include <tinynuget/exceptions.h>
#include <tinynuget/feed.h>
#include <tinynuget/package.h>
#include <tinynuget/semantic_version.h>
#include <tinynuget/version_range.h>

#include <algorithm>
#include <cctype>

namespace tinynuget
{
#if defined(_MSVC_STL_UPDATE)
    Feed::Feed() = default;
#endif

    Feed::Feed(std::unique_ptr<protocol::BasePackageService> packageService) : m_packageService(std::move(packageService))
    {
    }

    Feed::~Feed() = default;

    Feed::Feed(Feed&& other) noexcept = default;
    Feed& Feed::operator=(Feed&& other) noexcept = default;

    std::future<Package> Feed::resolvePackage(std::string_view packageName, const VersionRange& packageVersion)
    {
        std::string normalizedName = std::string{packageName};
        std::transform(normalizedName.begin(), normalizedName.end(), normalizedName.begin(), [](const char c) -> char {
            return static_cast<char>(std::tolower(c));
        });

        return std::async(std::launch::async, [this, normalizedName, packageVersion]() -> Package {
            try
            {
                auto semanticVersions = m_packageService->fetchVersions(normalizedName).get();
                std::sort(semanticVersions.begin(), semanticVersions.end(), &operator>);

                std::optional<SemanticVersion> matchingVersion;
                for (const auto& version : semanticVersions)
                {
                    if (packageVersion.in({version}))
                    {
                        matchingVersion = version;
                        break;
                    }
                }

                if (matchingVersion)
                {
                    return m_packageService->fetchInfo(normalizedName, *matchingVersion).get();
                }
                else
                {
                    VersionNotFoundException exc{std::move(normalizedName), std::move(packageVersion)};
                    throw exc;
                }
            }
            catch (HttpStatusException& httpException)
            {
                if (httpException.statusCode == 404)
                {
                    PackageNotFoundException exc{std::move(normalizedName)};
                    throw exc;
                }

                throw;
            }
        });
    }

} // namespace tinynuget

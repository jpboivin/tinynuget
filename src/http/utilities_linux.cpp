//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/client.h"
#include "http/utilities_linux.h"

#include <atomic>
#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <mutex>

#include <curl/curl.h>

namespace tinynuget::http
{
    ////////////////////////////////////////////////////////////////////
    std::atomic<bool> g_initialized = {false};
    std::mutex        g_initializationLock = {};

    void initializeCurl()
    {
        if (!g_initialized)
        {
            std::lock_guard lock(g_initializationLock);

            if (!g_initialized)
            {
                CURLcode ret = curl_global_init(CURL_GLOBAL_ALL);
                if (ret != CURLE_OK)
                    std::abort();

                std::atexit([]() { curl_global_cleanup(); });
                g_initialized = true;
            }
        }
    }

    void FILEDeleter::operator()(FILE* file) const
    {
        if (file != nullptr)
            std::fclose(file);
    }

    void CURLDeleter::operator()(CURL* curl) const
    {
        if (curl != nullptr)
            curl_easy_cleanup(curl);
    }

    ////////////////////////////////////////////////////////////////////
    std::future<void> download(std::string_view resourceUrl, std::string destination)
    {
        // ensure cURL is initialized
        initializeCurl();

        return std::async(std::launch::async, [resourceUrl = std::string{resourceUrl}, destination]() {
            SafeFILE destinationFile = SafeFILE{std::fopen(destination.c_str(), "wb")};
            if (destinationFile != nullptr)
            {
                SafeCURL curl = SafeCURL{curl_easy_init()};
                assert(curl.get() != nullptr);

                using write_callback_t = size_t (*)(char* ptr, size_t size, size_t count, void* userdata);
                write_callback_t writeCallback = [](char* data, const size_t size, const size_t count, void* userdata) -> size_t {
                    return std::fwrite(data, size, count, static_cast<FILE*>(userdata));
                };

                curl_easy_setopt(curl.get(), CURLOPT_URL, resourceUrl.c_str());
                curl_easy_setopt(curl.get(), CURLOPT_HTTPGET, 1);
                curl_easy_setopt(curl.get(), CURLOPT_WRITEDATA, destinationFile.get());
                curl_easy_setopt(curl.get(), CURLOPT_WRITEFUNCTION, writeCallback);

                const CURLcode result = curl_easy_perform(curl.get());
                if (result == CURLE_OK)
                {
                    long statusCode = 0;
                    curl_easy_getinfo(curl.get(), CURLINFO_RESPONSE_CODE, &statusCode);

                    if (statusCode >= 200 && statusCode <= 299)
                    {
                        // nothing to do
                        return;
                    }
                    else
                    {
                        HttpStatusException exc;
                        exc.resourceUrl = resourceUrl;
                        exc.statusCode = static_cast<uint16_t>(statusCode);

                        throw exc;
                    }
                }
                else
                {
                    CurlException exc;
                    exc.code = result;
                    exc.description = curl_easy_strerror(result);

                    throw exc;
                }
            }
        });
    }

} // namespace tinynuget::http

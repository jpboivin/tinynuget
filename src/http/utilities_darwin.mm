//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/utilities.h"

#include <future>
#include <memory>

#import <Foundation/Foundation.h>

namespace tinynuget::http
{
    std::future<void> download(std::string_view resourceUrl, std::string destination)
    {
        auto sharedPromise = std::make_shared<std::promise<void>>();

        NSURL* url = [NSURL URLWithString:[[NSString alloc] initWithBytes:resourceUrl.data()
                                                                   length:resourceUrl.size()
                                                                 encoding:NSUTF8StringEncoding]];

        NSString* destinationPath = [[NSString alloc] initWithBytes:destination.data()
                                                             length:destination.size()
                                                           encoding:NSUTF8StringEncoding];
        NSURL*    destinationURL = [NSURL fileURLWithPath:destinationPath];

        NSURLSession*             session = [NSURLSession sharedSession];
        NSURLSessionDownloadTask* downloadTask =
            [session downloadTaskWithURL:url
                       completionHandler:^(NSURL* location, NSURLResponse* response, NSError* error) {
                           if (!error)
                           {
                               NSHTTPURLResponse* httpResponse = static_cast<NSHTTPURLResponse*>(response);

                               const auto statusCode = [httpResponse statusCode];
                               if (statusCode >= 200 && statusCode <= 299)
                               {
                                   NSFileManager* fileManager = [NSFileManager defaultManager];

                                   if ([fileManager fileExistsAtPath:destinationPath])
                                       [fileManager removeItemAtURL:destinationURL error:nil];

                                   const BOOL success = [fileManager moveItemAtURL:location toURL:destinationURL error:&error];

                                   if (success)
                                   {
                                       sharedPromise->set_value();
                                   }
                                   else
                                   {
                                       NSErrorException exc;
                                       exc.code = [error code];
                                       exc.domain = [[error domain] UTF8String];
                                       exc.description = [[error localizedDescription] UTF8String];

                                       sharedPromise->set_exception(std::make_exception_ptr(exc));
                                   }
                               }
                               else
                               {
                                   HttpStatusException exc;
                                   exc.resourceUrl = resourceUrl;
                                   exc.statusCode = static_cast<uint16_t>(statusCode);

                                   sharedPromise->set_exception(std::make_exception_ptr(exc));
                               }
                           }
                           else
                           {
                               NSErrorException exc;
                               exc.code = [error code];
                               exc.domain = [[error domain] UTF8String];
                               exc.description = [[error localizedDescription] UTF8String];

                               sharedPromise->set_exception(std::make_exception_ptr(exc));
                           }
                       }];


        [downloadTask resume];
        return sharedPromise->get_future();
    }
}

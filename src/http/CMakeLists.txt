#
# Copyright (C) 2020 Jean-Philippe Boivin. All rights reserved.
#

set(SRC_FILES
    client.h
    client.cpp
    client_darwin.mm
    client_linux.cpp
    client_windows.cpp
    utilities.h
    utilities_linux.h
    utilities_darwin.mm
    utilities_linux.cpp
    utilities_windows.h
    utilities_windows.cpp
)
filter_platform_files(SRC_FILES)

target_sources(TinyNuget PRIVATE ${SRC_FILES})

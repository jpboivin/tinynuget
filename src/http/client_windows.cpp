//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/client.h"
#include "http/utilities_windows.h"

#include <windows.h>
#include <winhttp.h>

namespace tinynuget::http
{
    Client::Client(std::string_view baseAddress) : m_baseAddress(baseAddress)
    {
        if (!m_baseAddress.empty() && m_baseAddress.back() != '/')
            m_baseAddress += '/';
    }

    std::future<std::string> Client::fetchAsString(std::string resourceUrl, std::string contentType)
    {
        if (!resourceUrl.empty() && resourceUrl.front() == '/')
            resourceUrl = resourceUrl.substr(1);
        resourceUrl = m_baseAddress + resourceUrl;

        // TODO proper wide conversion

        return std::async(
            std::launch::async,
            [resourceUrl = std::wstring{resourceUrl.cbegin(), resourceUrl.cend()},
             contentType = std::wstring{contentType.cbegin(), contentType.cend()}]() -> std::string {
                LPCWSTR acceptTypes[] = {contentType.c_str(), nullptr};
                BOOL    success = true;

                URL_COMPONENTSW urlComponents;
                ZeroMemory(&urlComponents, sizeof(urlComponents));

                urlComponents.dwStructSize = sizeof(urlComponents);
                urlComponents.dwSchemeLength = static_cast<DWORD>(-1);
                urlComponents.dwHostNameLength = static_cast<DWORD>(-1);
                urlComponents.dwUrlPathLength = static_cast<DWORD>(-1);
                urlComponents.dwExtraInfoLength = static_cast<DWORD>(-1);

                success = WinHttpCrackUrl(resourceUrl.c_str(), static_cast<DWORD>(resourceUrl.size()), 0, &urlComponents);
                ensureSuccess(success);

                const auto secureScheme = std::wstring_view{urlComponents.lpszScheme, urlComponents.dwSchemeLength} == L"https";
                const auto hostname = std::wstring{urlComponents.lpszHostName, urlComponents.dwHostNameLength};
                const auto port = urlComponents.nPort;
                const auto path = std::wstring{urlComponents.lpszUrlPath, urlComponents.dwUrlPathLength} +
                                  std::wstring{urlComponents.lpszExtraInfo, urlComponents.dwExtraInfoLength};

                SafeHINTERNET session = SafeHINTERNET{WinHttpOpen(L"TinyNuget/1.0", //
                                                                  WINHTTP_ACCESS_TYPE_AUTOMATIC_PROXY,
                                                                  WINHTTP_NO_PROXY_NAME,
                                                                  WINHTTP_NO_PROXY_BYPASS,
                                                                  0)};
                ensureNotNull(session.get());

                SafeHINTERNET connection = SafeHINTERNET{WinHttpConnect(session.get(), //
                                                                        hostname.c_str(),
                                                                        port,
                                                                        0)};
                ensureNotNull(connection.get());

                SafeHINTERNET request = SafeHINTERNET{WinHttpOpenRequest(connection.get(),
                                                                         L"GET",
                                                                         path.c_str(),
                                                                         NULL,
                                                                         WINHTTP_NO_REFERER,
                                                                         acceptTypes,
                                                                         secureScheme ? WINHTTP_FLAG_SECURE : 0)};
                ensureNotNull(request.get());

                success = WinHttpSendRequest(request.get(), WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, NULL);
                ensureSuccess(success);

                success = WinHttpReceiveResponse(request.get(), NULL);
                ensureSuccess(success);

                DWORD statusCode = 0;
                DWORD statusCodeSize = sizeof(statusCode);
                success = WinHttpQueryHeaders(request.get(),
                                              WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER,
                                              WINHTTP_HEADER_NAME_BY_INDEX,
                                              &statusCode,
                                              &statusCodeSize,
                                              WINHTTP_NO_HEADER_INDEX);
                ensureSuccess(success);

                if (statusCode >= 200 && statusCode <= 299)
                {
                    std::string content;

                    constexpr DWORD bufferSize = 8192; // same as WinHTTP internal buffer
                    char            buffer[bufferSize];

                    DWORD readSize;
                    do
                    {
                        success = WinHttpReadData(request.get(), buffer, bufferSize, &readSize);
                        ensureSuccess(success);

                        content.append(buffer, readSize);
                    } while (readSize > 0);

                    return content;
                }
                else
                {
                    HttpStatusException exc;
                    exc.statusCode = static_cast<uint16_t>(statusCode);

                    throw exc;
                }
            });
    }

} // namespace tinynuget::http

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/client.h"
#include "json/json.h"
#include "xml/xml.h"

namespace tinynuget::http
{
    const std::string& Client::baseAddress() const noexcept
    {
        return m_baseAddress;
    }

    template<>
    std::future<JSON> Client::fetch<JSON>(std::string_view resourceUrl)
    {
        return std::async(std::launch::async, [this, resourceUrl = std::string{resourceUrl}]() -> JSON {
            static constexpr char content_type[] = "application/json";
            const auto            content = fetchAsString(std::move(resourceUrl), content_type).get();

            auto parsingError = std::string{};
            auto json = JSON::parse(content, parsingError);

            if (parsingError.empty())
            {
                return json;
            }
            else
            {
                JsonFormatException exc;
                exc.description = std::move(parsingError);

                throw exc;
            }
        });
    }

    template<>
    std::future<XMLPtr> Client::fetch<XMLPtr>(std::string_view resourceUrl)
    {
        return std::async(std::launch::async, [this, resourceUrl = std::string{resourceUrl}]() -> XMLPtr {
            static constexpr char content_type[] = "*/*"; // Note. Should be 'application/atom+xml' but some servers don't like it
            const auto            content = fetchAsString(std::move(resourceUrl), content_type).get();

            XMLPtr xml = std::make_unique<XML>();

            auto parsingError = xml->Parse(content.data(), content.size());
            if (parsingError == tinyxml2::XMLError::XML_SUCCESS)
            {
                return xml;
            }
            else
            {
                XmlFormatException exc;
                exc.code = parsingError;
                exc.description = XML::ErrorIDToName(parsingError);

                throw exc;
            }
        });
    }
} // namespace tinynuget::http

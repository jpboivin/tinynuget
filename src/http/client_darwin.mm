//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/client.h"

#import <Foundation/Foundation.h>

namespace tinynuget::http
{
    Client::Client(std::string_view baseAddress) : m_baseAddress(baseAddress)
    {
        if (!m_baseAddress.empty() && m_baseAddress.back() != '/')
            m_baseAddress += '/';
    }

    std::future<std::string> Client::fetchAsString(std::string resourceUrl, std::string contentType)
    {
        auto sharedPromise = std::make_shared<std::promise<std::string>>();

        if (!resourceUrl.empty() && resourceUrl.front() == '/')
            resourceUrl = resourceUrl.substr(1);
        resourceUrl = m_baseAddress + resourceUrl;

        NSURL* url = [NSURL URLWithString:[[NSString alloc] initWithBytes:resourceUrl.data()
                                                                   length:resourceUrl.size()
                                                                 encoding:NSUTF8StringEncoding]];

        NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:url];
        [request setValue:[[NSString alloc] initWithBytes:contentType.data() length:contentType.size() encoding:NSUTF8StringEncoding]
            forHTTPHeaderField:@"Accept"];

        NSURLSession*         session = [NSURLSession sharedSession];
        NSURLSessionDataTask* fetchTask =
            [session dataTaskWithRequest:request
                       completionHandler:^(NSData* data, NSURLResponse* response, NSError* error) {
                           if (!error)
                           {
                               NSHTTPURLResponse* httpResponse = static_cast<NSHTTPURLResponse*>(response);

                               const auto statusCode = [httpResponse statusCode];
                               if (statusCode >= 200 && statusCode <= 299)
                               {
                                   // Warning: NSString decoding is strict -- if one codepoint is invalid, the whole data will be rejected
                                   NSString* content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                                   sharedPromise->set_value(content != nullptr ? [content UTF8String] : std::string{});
                               }
                               else
                               {
                                   HttpStatusException exc;
                                   exc.resourceUrl = resourceUrl;
                                   exc.statusCode = static_cast<uint16_t>(statusCode);

                                   sharedPromise->set_exception(std::make_exception_ptr(exc));
                               }
                           }
                           else
                           {
                               NSErrorException exc;
                               exc.code = [error code];
                               exc.domain = [[error domain] UTF8String];
                               exc.description = [[error localizedDescription] UTF8String];

                               sharedPromise->set_exception(std::make_exception_ptr(exc));
                           }
                       }];


        [fetchTask resume];
        return sharedPromise->get_future();
    }
}

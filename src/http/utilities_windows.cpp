//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/utilities.h"
#include "http/utilities_windows.h"

#include <cstdio>
#include <cstdlib>

#include <windows.h>
#include <winhttp.h>

namespace tinynuget::http
{
    ////////////////////////////////////////////////////////////////////
    void FILEDeleter::operator()(FILE* file) const
    {
        if (file != nullptr)
            std::fclose(file);
    }

    void HINTERNETDeleter::operator()(HINTERNET handle) const
    {
        if (handle != NULL)
            WinHttpCloseHandle(handle);
    }

    ////////////////////////////////////////////////////////////////////
    std::string errorMessage(const DWORD errorCode)
    {
        LPSTR messageData = NULL;
        DWORD messageSize = FormatMessageA(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
                                           NULL,
                                           errorCode,
                                           MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
                                           reinterpret_cast<LPSTR>(&messageData),
                                           0,
                                           NULL);

        std::string message = {messageData, messageSize};
        LocalFree(messageData);

        return message;
    }

    void ensureNotNull(const HINTERNET handle)
    {
        if (handle == NULL)
        {
            WinHttpException exc;
            exc.code = GetLastError();
            exc.description = errorMessage(exc.code);

            throw exc;
        }
    }

    void ensureSuccess(const BOOL success)
    {
        if (!success)
        {
            WinHttpException exc;
            exc.code = GetLastError();
            exc.description = errorMessage(exc.code);

            throw exc;
        }
    }

    ////////////////////////////////////////////////////////////////////
    std::future<void> download(std::string_view resourceUrl, std::string destination)
    {
        // TODO proper wide conversion
        return std::async(std::launch::async, [resourceUrl = std::wstring{resourceUrl.cbegin(), resourceUrl.cend()}, destination]() {
            SafeFILE destinationFile = SafeFILE{std::fopen(destination.c_str(), "wb")};
            if (destinationFile != nullptr)
            {
                BOOL success = true;

                URL_COMPONENTSW urlComponents;
                ZeroMemory(&urlComponents, sizeof(urlComponents));

                urlComponents.dwStructSize = sizeof(urlComponents);
                urlComponents.dwSchemeLength = static_cast<DWORD>(-1);
                urlComponents.dwHostNameLength = static_cast<DWORD>(-1);
                urlComponents.dwUrlPathLength = static_cast<DWORD>(-1);
                urlComponents.dwExtraInfoLength = static_cast<DWORD>(-1);

                success = WinHttpCrackUrl(resourceUrl.c_str(), static_cast<DWORD>(resourceUrl.size()), 0, &urlComponents);
                ensureSuccess(success);

                const auto secureScheme = std::wstring_view{urlComponents.lpszScheme, urlComponents.dwSchemeLength} == L"https";
                const auto hostname = std::wstring{urlComponents.lpszHostName, urlComponents.dwHostNameLength};
                const auto port = urlComponents.nPort;
                const auto path = std::wstring{urlComponents.lpszUrlPath, urlComponents.dwUrlPathLength} +
                                  std::wstring{urlComponents.lpszExtraInfo, urlComponents.dwExtraInfoLength};

                SafeHINTERNET session = SafeHINTERNET{WinHttpOpen(L"TinyNuget/1.0", //
                                                                  WINHTTP_ACCESS_TYPE_AUTOMATIC_PROXY,
                                                                  WINHTTP_NO_PROXY_NAME,
                                                                  WINHTTP_NO_PROXY_BYPASS,
                                                                  0)};
                ensureNotNull(session.get());

                SafeHINTERNET connection = SafeHINTERNET{WinHttpConnect(session.get(), //
                                                                        hostname.c_str(),
                                                                        port,
                                                                        0)};
                ensureNotNull(connection.get());

                SafeHINTERNET request = SafeHINTERNET{WinHttpOpenRequest(connection.get(),
                                                                         L"GET",
                                                                         path.c_str(),
                                                                         NULL,
                                                                         WINHTTP_NO_REFERER,
                                                                         WINHTTP_DEFAULT_ACCEPT_TYPES,
                                                                         secureScheme ? WINHTTP_FLAG_SECURE : 0)};
                ensureNotNull(request.get());

                success = WinHttpSendRequest(request.get(), WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, 0, NULL);
                ensureSuccess(success);

                success = WinHttpReceiveResponse(request.get(), NULL);
                ensureSuccess(success);

                DWORD statusCode = 0;
                DWORD statusCodeSize = sizeof(statusCode);
                success = WinHttpQueryHeaders(request.get(),
                                              WINHTTP_QUERY_STATUS_CODE | WINHTTP_QUERY_FLAG_NUMBER,
                                              WINHTTP_HEADER_NAME_BY_INDEX,
                                              &statusCode,
                                              &statusCodeSize,
                                              WINHTTP_NO_HEADER_INDEX);
                ensureSuccess(success);

                if (statusCode >= 200 && statusCode <= 299)
                {
                    constexpr DWORD bufferSize = 8192; // same as WinHTTP internal buffer
                    uint8_t         buffer[bufferSize];

                    DWORD readSize;
                    do
                    {
                        success = WinHttpReadData(request.get(), buffer, bufferSize, &readSize);
                        ensureSuccess(success);

                        std::fwrite(buffer, sizeof(uint8_t), readSize, destinationFile.get());
                    } while (readSize > 0);

                    return;
                }
                else
                {
                    HttpStatusException exc;
                    exc.statusCode = static_cast<uint16_t>(statusCode);

                    throw exc;
                }
            }
        });
    }

} // namespace tinynuget::http

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"
#include "http/client.h"
#include "http/utilities_linux.h"

#include <cassert>

#include <curl/curl.h>

namespace tinynuget::http
{
    Client::Client(std::string_view baseAddress) : m_baseAddress(baseAddress)
    {
        if (!m_baseAddress.empty() && m_baseAddress.back() != '/')
            m_baseAddress += '/';

        // ensure cURL is initialized
        initializeCurl();
    }

    std::future<std::string> Client::fetchAsString(std::string resourceUrl, std::string contentType)
    {
        if (!resourceUrl.empty() && resourceUrl.front() == '/')
            resourceUrl = resourceUrl.substr(1);
        resourceUrl = m_baseAddress + resourceUrl;

        return std::async(std::launch::async, [resourceUrl = std::move(resourceUrl), contentType = std::move(contentType)]() -> std::string {
            std::string content;
            std::string acceptHeader = "Accept: " + contentType;

            struct curl_slist* headers = nullptr;
            headers = curl_slist_append(headers, acceptHeader.c_str());

            SafeCURL curl = SafeCURL{curl_easy_init()};
            assert(curl.get() != nullptr);

            using write_callback_t = size_t (*)(char* ptr, size_t size, size_t count, void* userdata);
            write_callback_t writeCallback = [](char* data, const size_t size, const size_t count, void* userdata) -> size_t {
                static_cast<std::string*>(userdata)->append(static_cast<const char*>(data), size * count);
                return count;
            };

            curl_easy_setopt(curl.get(), CURLOPT_URL, resourceUrl.c_str());
            curl_easy_setopt(curl.get(), CURLOPT_HTTPGET, 1);
            curl_easy_setopt(curl.get(), CURLOPT_HTTPHEADER, headers);
            curl_easy_setopt(curl.get(), CURLOPT_WRITEDATA, &content);
            curl_easy_setopt(curl.get(), CURLOPT_WRITEFUNCTION, writeCallback);

            const CURLcode result = curl_easy_perform(curl.get());
            if (result == CURLE_OK)
            {
                long statusCode = 0;
                curl_easy_getinfo(curl.get(), CURLINFO_RESPONSE_CODE, &statusCode);

                if (statusCode >= 200 && statusCode <= 299)
                {
                    return content;
                }
                else
                {
                    HttpStatusException exc;
                    exc.resourceUrl = resourceUrl;
                    exc.statusCode = static_cast<uint16_t>(statusCode);

                    throw exc;
                }
            }
            else
            {
                CurlException exc;
                exc.code = result;
                exc.description = curl_easy_strerror(result);

                throw exc;
            }
        });
    }

} // namespace tinynuget::http

//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "http/client.h"
#include "http/utilities.h"
#include "utilities/base64.h"
#include "utilities/filesystem.h"
#include "utilities/sha512.h"
#include "utilities/zip.h"

#include <tinynuget/package.h>

#include <cstdio>
#include <string>

#include <filesystem>

namespace tinynuget
{
    struct ZipExtractionException : std::exception
    {
        const char* what() const noexcept override
        {
            return "ZipExtractionException";
        }
    };

    Package::Package(std::string name, SemanticVersion version, std::string url, std::optional<std::string> hash)
        : m_name(std::move(name)), m_version(std::move(version)), m_url(std::move(url)), m_hash(std::move(hash))
    {
    }

    std::string_view Package::name() const
    {
        return m_name;
    }

    SemanticVersion Package::version() const
    {
        return m_version;
    }

    bool Package::installed() const
    {
        return installedIn(filesystem::nuget::globalDirectory());
    }

    bool Package::installedIn(std::string_view installDir) const
    {
        if (installDir.empty())
            return false;

        std::string packageName = m_name + '.' + m_version.normalize() + ".nupkg";
        std::string packagePath = filesystem::combine(directoryFor(installDir), packageName);

        return filesystem::exists(packagePath);
    }

    std::string Package::directory() const
    {
        return directoryFor(filesystem::nuget::globalDirectory());
    }

    std::string Package::directoryFor(std::string_view installDir) const
    {
        return filesystem::combine(installDir, m_name, m_version.normalize());
    }

    void Package::install()
    {
        if (installed())
            return;

        std::string workingDirectory = filesystem::combine(filesystem::nuget::tempDirectory(), m_name, m_version.normalize());
        filesystem::ensureDirectoryExists(workingDirectory);

        std::string packageName = m_name + '.' + m_version.normalize() + ".nupkg";
        std::string packageResource = '/' + m_name + '/' + m_version.normalize() + '/' + packageName;
        std::string packagePath = filesystem::combine(workingDirectory, packageName);

        // 1. Fetch the .nuget package
        http::download(m_url, packagePath).get();

        // 2. Compute the hash of the .nuget package (unused by TinyNuget)
        computeHash(packagePath);

        // 3. Extract the package
        if (zip::extract(packagePath, workingDirectory))
        {
            filesystem::remove(filesystem::combine(workingDirectory, "[Content_Types].xml"));
            filesystem::removeAll(filesystem::combine(workingDirectory, "_rels/"));
            filesystem::removeAll(filesystem::combine(workingDirectory, "package/"));
        }
        else
        {
            throw ZipExtractionException{};
        }

        // 4. Move to global packages
        std::string destinationPackageDirectory = filesystem::combine(filesystem::nuget::globalDirectory(), m_name);
        std::string destinationDirectory = filesystem::combine(destinationPackageDirectory, m_version.normalize());
        filesystem::ensureDirectoryExists(destinationPackageDirectory);

        const int moveRet = std::rename(workingDirectory.c_str(), destinationDirectory.c_str());

        // 5. Package got installed in the meantime...
        if (moveRet != 0 && filesystem::exists(destinationDirectory))
        {
            // TODO validate hashes?
        }
    }

    void Package::installIn(std::string_view installDir)
    {
        if (installedIn(installDir))
            return;

        // 1. Install the global package
        if (!installed())
            install();

        std::string sourcePackageDirectory = filesystem::combine(filesystem::nuget::globalDirectory(), m_name);
        std::string sourceDirectory = filesystem::combine(sourcePackageDirectory, m_version.normalize());

        std::string destinationPackageDirectory = filesystem::combine(installDir, m_name);
        std::string destinationDirectory = filesystem::combine(destinationPackageDirectory, m_version.normalize());
        std::string temporaryDestinationDirectory = filesystem::combine(destinationPackageDirectory, "");
        filesystem::ensureDirectoryExists(destinationPackageDirectory);
        filesystem::ensureDirectoryExists(temporaryDestinationDirectory);

        // 2. Copy the global package
        std::filesystem::copy(std::filesystem::u8path(sourceDirectory), std::filesystem::u8path(temporaryDestinationDirectory));

        // 3. Move to destination
        const int moveRet = std::rename(temporaryDestinationDirectory.c_str(), destinationDirectory.c_str());

        // 4. Package got installed in the meantime...
        if (moveRet != 0 && filesystem::exists(destinationDirectory))
        {
            filesystem::removeAll(temporaryDestinationDirectory);
            // TODO validate hashes?
        }
    }

    void Package::computeHash(const std::string& packagePath) const
    {
        std::string packageHashPath = packagePath + ".sha512";

        FILE*   stream;
        uint8_t digest[hash::SHA512::digest_size];

        stream = std::fopen(packagePath.c_str(), "rb");
        if (stream != nullptr)
        {
            uint8_t buffer[4096];
            size_t  read = 0;

            hash::SHA512 hash;
            while ((read = std::fread(buffer, sizeof(buffer[0]), sizeof(buffer) / sizeof(buffer[0]), stream)) != 0)
                hash.update(buffer, read);
            hash.finalize(digest);

            std::fclose(stream);
        }

        stream = std::fopen(packageHashPath.c_str(), "wt");
        if (stream != nullptr)
        {
            std::string base64Digest = base64::encode(digest, sizeof(digest));

            std::fwrite(base64Digest.data(), base64Digest.size(), sizeof(std::string::value_type), stream);
            std::fclose(stream);
        }
    }

} // namespace tinynuget

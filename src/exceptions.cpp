//
//  MIT License
//
//  Copyright (c) Jean-Philippe Boivin <contact@jpboivin.me>
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all
//  copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
//  SOFTWARE.
//

#include "exceptions.h"

#include <tinynuget/exceptions.h>

namespace tinynuget
{
    ////////////////////////////////////////////////////////////////////
    UnsupportedProtocolException::UnsupportedProtocolException(ProtocolVersion version) noexcept : m_version(version)
    {
    }

    ProtocolVersion UnsupportedProtocolException::version() const noexcept
    {
        return m_version;
    }

    const char* UnsupportedProtocolException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message,
                      sizeof(message),
                      "UnsupportedProtocolException[version=%u]",
                      static_cast<std::underlying_type_t<ProtocolVersion>>(m_version));
        return message;
    }

    UnsupportedFeedException::UnsupportedFeedException(std::string address) : m_address(std::move(address))
    {
    }

    std::string_view UnsupportedFeedException::address() const noexcept
    {
        return m_address;
    }

    const char* UnsupportedFeedException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "UnsupportedFeedException[address=%s]", m_address.c_str());
        return message;
    }

    PackageNotFoundException::PackageNotFoundException(std::string name) : m_name(std::move(name))
    {
    }

    std::string_view PackageNotFoundException::name() const noexcept
    {
        return m_name;
    }


    const char* PackageNotFoundException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "PackageNotFoundException[name='%s']", m_name.c_str());
        return message;
    }

    VersionNotFoundException::VersionNotFoundException(std::string name, VersionRange range)
        : m_name(std::move(name)), m_range(std::move(range))
    {
    }

    std::string_view VersionNotFoundException::name() const noexcept
    {
        return m_name;
    }

    const VersionRange& VersionNotFoundException::range() const noexcept
    {
        return m_range;
    }

    const char* VersionNotFoundException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "VersionNotFoundException[name='%s', range='%s']", m_name.c_str(), m_range.toString().c_str());
        return message;
    }

    ////////////////////////////////////////////////////////////////////
    const char* NSErrorException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message,
                      sizeof(message),
                      "NSErrorException[code=%lld, domain=%s, desc='%s']",
                      static_cast<long long>(code),
                      domain.c_str(),
                      description.c_str());
        return message;
    }

    const char* CurlException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "CurlException[code=%d, desc='%s']", code, description.c_str());
        return message;
    }

    const char* WinHttpException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "WinHttpException[code=%lu, desc='%s']", code, description.c_str());
        return message;
    }

    const char* HttpStatusException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "HttpStatusException[resource='%s', statusCode=%u]", resourceUrl.c_str(), statusCode);
        return message;
    }

    const char* JsonFormatException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "JsonFormatException[desc='%s']", description.c_str());
        return message;
    }

    const char* XmlFormatException::what() const noexcept
    {
        static thread_local char message[2048];
        std::snprintf(message, sizeof(message), "XmlFormatException[code=%d, desc='%s']", code, description.c_str());
        return message;
    }

} // namespace tinynuget

# TinyNuget

TinyNuget is a tiny C++ library providing a minimalistic implementation of the NuGet protocol. It aims to be used in tooling that cannot rely on a .NET runtime.

## Features

- Package retrieval from NuGet v2 and NuGet v3 feeds
- Package installation (global and to a specific destination)
- Support version ranges when resolving packages
- Support floating versions when resolving packages
- Minimal runtime dependencies when linked statically

# Requirements

TinyNuget requires Git, CMake 3.13+ and C++17 compiler. No other specific tool should be required to compile the library.

TinyNuget depends on:
- `TinyProcessLibrary`
- `TinyXML-2` (when support for protocol v2 is enabled)
- `json11` (when support for protocol v3 is enabled)
- `minizip` (when support for minizip is enabled)

These dependencies were choosen for their small footprint and easy integration.

**All dependencies are provided as Git submodules.**

# Runtime Requirements

TinyNuget is designed to be linked statically while keeping a small code size for the resulting executable. It is also designed to have minimal runtime dependencies (it should run anywhere easily). To achieve that, system APIs are used whenever possible or tiny libraries (like the dependencies are) are linked statically.

## Apple macOS

On macOS, TinyNuget depends on several system libraries (like `CoreFoundation`). Outside of these system libraries, that should always be available, TinyNuget also depends on the `unzip` tool. This tool is provided with the system and should always be available.

It is possible to use `minizip` instead by setting `TINYNUGET_ENABLE_MINIZIP` to `ON`, but it isn't recommended as the final code size will grow and ZIP-related bugs won't be fixed by a simple system update.

## GNU/Linux

On Linux, TinyNuget depends on several system libraries (like `glibc` or the Linux kernel itself). Outside of these system libraries, it also depends on widely available libraries (like `libz`) that should always be available. Finally, due to the lack of a standard HTTP library in the distributions, `libcurl` is used. Some distributions (like CentOS) will always have `libcurl` installed, while other distributions (like Ubuntu and Debian) does not seem to always have it, at least in their slim images. On these distributions, `libcurl3` (sometimes named `libcurl4` due to the API, but not ABI, break) must be installed.

By default, TinyNuget will use `minizip` to handle ZIP files. It is also possible to use `unzip` by setting `TINYNUGET_ENABLE_MINIZIP` to `OFF`, but it isn't recommended as the tool is not widely installed.

The examples of TinyNuget are built using a special Docker image that provides cross-distribution portability. People familiar with the issue have likely seen errors in the past when running an ELF file on another distribution (or version) due to, for example, `glibc` version mismatch. This Docker image provides tooling to produce cross-distribution compatible executables (on the condition that the ABI remains stable). Based on CentOS 6, the resulting libraries and executables should be compatible with: Debian 7+, Ubuntu 10.10+, RHEL 6+ and CentOS 6+. Distributions based on `musl` (like Alpine Linux) won't be compatible. There's no solution for that as all efforts to have a standard for cross-distribution executables have failed as far as I know.

## Microsoft Windows

On Windows, TinyNuget depends on several system libraries (like `bcrypt`, `shell32`, `winhttp`, etc). Outside of these system libraries, that should always be available, TinyNuget also depends on the `MSVC` runtime. This should be the only dependency to install on a barebone system.

# Supported Platforms

The following platforms are supported:

- Apple macOS
- GNU/Linux
- Microsoft Windows

Porting the library to other Unix-like platforms should be relatively simple.

# F.A.Q.

## 1. Is it safe to concurrently install the same NuGet package using TinyNuget?

Yes. It should be if the NuGet scratch folder is on the same partition as the global package directory.

# License

TinyNuget is distributed under the OSI-approved MIT License. See [LICENSE](LICENSE) for more details.

